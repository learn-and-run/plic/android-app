package fr.learnandrun.android.view.ui.fragment

import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import fr.learnandrun.android.viewmodel.alert.Alert

class AlertDialogFragment : DialogFragment() {

    var alertView: View? = null
    lateinit var alert: Alert<*>
    var destroyingView = false

    override fun onAttach(context: Context) {
        super.onAttach(context)
        destroyingView = false
        alert = (targetFragment as BasicFragment<*>?)?.viewModel?.alert
            ?: throw IllegalStateException("There is no alert to display")
    }

    override fun onResume() {
        super.onResume()
        alert.dialogDismiss = ::dismiss
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        super.onCreate(savedInstanceState)
        val view = layoutInflater.inflate(alert.layoutId, null)
        alertView = view
        return AlertDialog.Builder(requireContext()).setView(view).create().apply {
            window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        alertView?.let {
            alert.setupView(this, it)
        }
        return alertView
    }

    override fun onDestroyView() {
        destroyingView = true
        super.onDestroyView()
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        if (!destroyingView)
            alert.dismiss()
    }

}