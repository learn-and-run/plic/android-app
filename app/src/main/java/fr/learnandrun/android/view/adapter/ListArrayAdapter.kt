package fr.learnandrun.android.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

class ListArrayAdapter<ItemType : Any, ViewHolderType : RecyclerView.ViewHolder>(
    private val itemLayoutId: Int,
    private val createViewHolder: (view: View) -> ViewHolderType,
    private val setupViewHolder: (holder: ViewHolderType, item: ItemType) -> Unit,
    private val itemList: List<ItemType>
) : RecyclerView.Adapter<ViewHolderType>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderType {
        val rowView: View = LayoutInflater.from(parent.context).inflate(itemLayoutId, parent, false)
        return createViewHolder(rowView)
    }

    override fun onBindViewHolder(holder: ViewHolderType, position: Int) {
        setupViewHolder(holder, itemList[position])
    }

    override fun getItemCount(): Int = itemList.size

}