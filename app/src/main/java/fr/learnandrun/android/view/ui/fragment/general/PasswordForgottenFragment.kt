package fr.learnandrun.android.view.ui.fragment.general

import android.os.Bundle
import android.view.View
import fr.learnandrun.android.R
import fr.learnandrun.android.tools.setPushAndOnClick
import fr.learnandrun.android.view.ui.fragment.BasicFragment
import fr.learnandrun.android.viewmodel.general.PasswordForgottenViewModel
import kotlinx.android.synthetic.main.fragment_password_forgotten.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class PasswordForgottenFragment
    : BasicFragment<PasswordForgottenViewModel>(R.layout.fragment_password_forgotten) {

    override val viewModel: PasswordForgottenViewModel by viewModel()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.apply {
            emailLiveText.bindEditTextView(
                viewLifecycleOwner,
                fragment_password_forgotten_email_field.editText
            )
            sendButtonIsActiveLive.observe(viewLifecycleOwner) {
                fragment_password_forgotten_send_button.setActive(it)
            }
            fragment_password_forgotten_send_button.setPushAndOnClick {
                send()
            }
            fragment_password_forgotten_cancel_button.setPushAndOnClick {
                cancel()
            }
        }
    }

}