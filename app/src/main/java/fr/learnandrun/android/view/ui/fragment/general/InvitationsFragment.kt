package fr.learnandrun.android.view.ui.fragment.general

import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.lifecycle.viewModelScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import fr.learnandrun.android.R
import fr.learnandrun.android.tools.setPushAndOnClick
import fr.learnandrun.android.view.adapter.ListArrayAdapter
import fr.learnandrun.android.view.ui.element.decoration.SimpleDividerItemDecoration
import fr.learnandrun.android.view.ui.fragment.BasicFragment
import fr.learnandrun.android.viewmodel.general.InvitationsViewModel
import kotlinx.android.synthetic.main.fragment_invitations.*
import kotlinx.android.synthetic.main.invitation_row.view.*
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel

class InvitationsFragment : BasicFragment<InvitationsViewModel>(R.layout.fragment_invitations) {

    override val viewModel: InvitationsViewModel by viewModel()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.apply {
            isRefreshingLive.observe(viewLifecycleOwner) {
                fragment_invitation_swipe_list_refresh_layout.isRefreshing = it
                if (it)
                    fragment_invitation_list_empty.visibility = View.GONE
                else {
                    if (listInvitations?.isEmpty() == true) {
                        fragment_invitation_list_empty.visibility = View.VISIBLE
                        fragment_invitation_list.visibility = View.GONE
                    } else {
                        fragment_invitation_list_empty.visibility = View.GONE
                        fragment_invitation_list.visibility = View.VISIBLE
                    }
                }
            }
            fragment_invitation_swipe_list_refresh_layout.setOnRefreshListener {
                viewModelScope.launch {
                    refreshInvitations()
                }
            }
            fragment_invitation_top_bar_back_button.setPushAndOnClick {
                back()
            }
            fragment_invitation_list.apply {
                addItemDecoration(
                    SimpleDividerItemDecoration(
                        ContextCompat.getColor(requireContext(), R.color.colorGreen),
                        2
                    )
                )
                setHasFixedSize(true)
                layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
            }
            class InvitationViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
                val pseudoText = itemView.invitation_row_pseudo
                val acceptButton = itemView.invitation_row_accept_button
                val declineButton = itemView.invitation_row_decline_button
            }
            listInvitationsLive.observe(viewLifecycleOwner) { listInvitations ->
                listInvitations ?: return@observe
                fragment_invitation_list.adapter = ListArrayAdapter(
                    R.layout.invitation_row,
                    { InvitationViewHolder(it) },
                    { holder, invitationModel ->
                        holder.pseudoText.text = invitationModel.pseudo
                        holder.acceptButton.setPushAndOnClick {
                            acceptInvitation(invitationModel)
                        }
                        holder.declineButton.setPushAndOnClick {
                            declineInvitation(invitationModel)
                        }
                    },
                    listInvitations
                )

            }
        }
    }
}