package fr.learnandrun.android.view.ui.fragment.student

import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.lifecycle.viewModelScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import fr.learnandrun.android.R
import fr.learnandrun.android.service.model.user.StudentModel
import fr.learnandrun.android.tools.setPushAndOnClick
import fr.learnandrun.android.view.adapter.ListArrayAdapter
import fr.learnandrun.android.view.ui.element.decoration.SimpleDividerItemDecoration
import fr.learnandrun.android.view.ui.fragment.BasicFragment
import fr.learnandrun.android.viewmodel.student.StudentFriendsViewModel
import kotlinx.android.synthetic.main.fragment_student_friends.*
import kotlinx.android.synthetic.main.person_row.view.*
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel

class StudentFriendsFragment : BasicFragment<StudentFriendsViewModel>(R.layout.fragment_student_friends) {

    override val viewModel: StudentFriendsViewModel by viewModel()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        viewModel.apply {
            refreshingLive.observe(viewLifecycleOwner) {
                fragment_student_friend_swipe_list_refresh_layout.isRefreshing = it
                if (it)
                    fragment_student_friend_empty_list_text_view.visibility = View.GONE
                else {
                    if (listStudentRelations?.isEmpty() == true) {
                        fragment_student_friend_list_view.visibility = View.GONE
                        fragment_student_friend_empty_list_text_view.visibility = View.VISIBLE
                    } else {
                        fragment_student_friend_list_view.visibility = View.VISIBLE
                        fragment_student_friend_empty_list_text_view.visibility = View.GONE
                    }
                }
            }
            fragment_student_friend_top_bar_cup_button.setPushAndOnClick {
                ladder()
            }
            fragment_student_friend_add_friend_button.setPushAndOnClick {
                addStudent()
            }
            fragment_student_friend_swipe_list_refresh_layout.setOnRefreshListener {
                viewModelScope.launch {
                    refreshStudents()
                }
            }
            fragment_student_friend_top_bar_back_button.setPushAndOnClick {
                back()
            }
            fragment_student_friend_list_view.apply {
                addItemDecoration(
                    SimpleDividerItemDecoration(
                        ContextCompat.getColor(requireContext(), R.color.colorGreen),
                        2
                    )
                )
                setHasFixedSize(true)
                layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
            }
            class FriendViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
                val rowLayout = itemView.person_row_layout
                val trashButton = itemView.person_row_swipe_trash_button
                val parentPseudoTextView = itemView.person_row_name_text
                val infoTextView = itemView.person_row_info_text
            }
            listStudentRelationsLive.observe(viewLifecycleOwner) { listStudentRelations ->
                listStudentRelations ?: return@observe
                fragment_student_friend_list_view.adapter = ListArrayAdapter(
                    R.layout.person_row,
                    { FriendViewHolder(it) },
                    { holder, studentRelation ->
                        val studentModel = studentRelation.userModel as StudentModel
                        holder.trashButton.setPushAndOnClick {
                            studentTrash(studentRelation)
                        }
                        holder.parentPseudoTextView.text = studentModel.pseudo
                        holder.infoTextView.visibility = View.GONE
                        holder.rowLayout.setPushAndOnClick {
                            studentClick(studentModel)
                        }
                    },
                    listStudentRelations
                )
            }
        }
    }

}