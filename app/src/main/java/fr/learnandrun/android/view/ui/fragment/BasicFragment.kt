package fr.learnandrun.android.view.ui.fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.view.ContextThemeWrapper
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import fr.learnandrun.android.viewmodel.BasicViewModel


/**
 * A basic fragment that simplify the declaration of a fragment
 */
abstract class BasicFragment<VM : BasicViewModel>(
    private val layoutResource: Int,
    private val themeId: Int? = null,
    private val statusColorId: Int? = null,
    private val navigationColorId: Int? = null
) : Fragment() {

    abstract val viewModel: VM

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var localInflater = inflater
        themeId?.let {
            val contextThemeWrapper: Context = ContextThemeWrapper(activity, themeId)
            localInflater = inflater.cloneInContext(contextThemeWrapper)
        }
        navigationColorId?.let {
            requireActivity().window.navigationBarColor =
                ContextCompat.getColor(requireContext(), navigationColorId)
        }
        statusColorId?.let {
            requireActivity().window.statusBarColor =
                ContextCompat.getColor(requireContext(), statusColorId)
        }
        // Inflate the layout for this fragment
        return localInflater.inflate(layoutResource, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.linkBasicEventToFragment(this, view)
    }
}