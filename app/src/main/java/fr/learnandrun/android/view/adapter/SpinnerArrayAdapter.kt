package fr.learnandrun.android.view.adapter

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.SpinnerAdapter
import android.widget.TextView
import androidx.core.content.ContextCompat
import fr.learnandrun.android.R
import fr.learnandrun.android.view.ui.element.spinner.SpinnerView

class SpinnerArrayAdapter<T>(
    context: Context,
    val spinnerView: SpinnerView,
    val items: List<T>,
    val getDisplayName: T.() -> String,
    val getBackColor: (T.() -> Int)? = null,
    val getForeColor: (T.() -> Int)? = null,
    val onCurrentChange: ((T) -> Unit)? = null
) : BaseAdapter(), SpinnerAdapter {

    override fun getItem(position: Int) = items[position]
    override fun getItemId(position: Int) = position.toLong()
    override fun getCount() = items.size

    var currentPosition = 0

    fun getCurrentItem() = items[currentPosition]

    val itemsView = items.mapIndexed { position, item ->
        View.inflate(context, R.layout.spinner_item, null).apply {
            val spinnerItem = findViewById<TextView>(R.id.spinner_item)
            getBackColor?.let { getBackColor ->
                when (position) {
                    0 -> spinnerItem.background =
                        ContextCompat.getDrawable(context, R.drawable.rectangle_round_up)
                    items.size - 1 -> spinnerItem.background =
                        ContextCompat.getDrawable(context, R.drawable.rectangle_round_down)
                    else -> spinnerItem.background =
                        ContextCompat.getDrawable(context, R.drawable.rectangle)
                }
                background.setTint(
                    ContextCompat.getColor(context, item.getBackColor())
                )
            }
            getForeColor?.let { getForeColor ->
                spinnerItem.setTextColor(
                    ContextCompat.getColor(context, item.getForeColor())
                )
            }
            spinnerItem.text = item.getDisplayName()
            setOnClickListener {
                select(position)
                spinnerView.observableSpinner.isOpen = false
                spinnerView.observableSpinner.onDetachedFromWindow()
            }
        }
    }

    fun select(position: Int) {
        currentPosition = position
        spinnerView.textView.text = getCurrentItem().getDisplayName()
        onCurrentChange?.invoke(getCurrentItem())
    }

    fun select(item: T) {
        val position = items.indexOf(item)
        check(position != -1)
        select(position)
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        return convertView ?: itemsView[currentPosition]
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup?): View {
        return convertView ?: itemsView[position]

    }
}