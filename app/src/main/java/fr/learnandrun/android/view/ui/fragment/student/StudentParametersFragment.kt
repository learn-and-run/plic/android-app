package fr.learnandrun.android.view.ui.fragment.student

import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.lifecycle.viewModelScope
import fr.learnandrun.android.R
import fr.learnandrun.android.service.model.user.LevelType
import fr.learnandrun.android.tools.setPushAndOnClick
import fr.learnandrun.android.view.adapter.SpinnerArrayAdapter
import fr.learnandrun.android.view.ui.fragment.BasicFragment
import fr.learnandrun.android.viewmodel.student.StudentParametersViewModel
import kotlinx.android.synthetic.main.fragment_student_parameters.*
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel

class StudentParametersFragment : BasicFragment<StudentParametersViewModel>(R.layout.fragment_student_parameters) {

    override val viewModel: StudentParametersViewModel by viewModel()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.apply {

            firstNameFieldLive.bindEditTextView(
                viewLifecycleOwner,
                fragment_student_parameters_firstname_field.editText
            )
            lastNameFieldLive.bindEditTextView(
                viewLifecycleOwner,
                fragment_student_parameters_lastname_field.editText
            )
            pseudoFieldLive.bindEditTextView(
                viewLifecycleOwner,
                fragment_student_parameters_pseudo_field.editText
            )

            fragment_student_parameters_classe_spinner.apply spinnerView@{
                observableSpinner.apply observableSpinner@{
                    setPopupBackgroundDrawable(
                        ContextCompat.getDrawable(requireContext(), R.drawable.rectangle_rounded)
                    )
                    popupBackground.setTint(
                        ContextCompat.getColor(requireContext(), R.color.colorYellow)
                    )
                    val adapter = SpinnerArrayAdapter(
                        requireContext(),
                        this@spinnerView,
                        LevelType.values().toList(),
                        LevelType::displayName,
                        getBackColor = { R.color.colorGreen },
                        getForeColor = { R.color.colorWhite }
                    ) {
                        viewModel.classe = it
                    }
                    this.adapter = adapter
                    viewModel.viewModelScope.launch {
                        delay(1)
                        dropDownHorizontalOffset = fragment_student_parameters_classe_spinner.measuredWidth
                        adapter.select(classe)
                    }
                }
            }
            validateIsActiveLive.observe(viewLifecycleOwner) {
                fragment_student_parameters_validate_button.setActive(it)
            }
            fragment_student_parameters_validate_button.setPushAndOnClick {
                validate()
            }
            fragment_student_parameters_top_bar_back_button.setPushAndOnClick {
                back()
            }
            fragment_student_parameters_change_email_button.setPushAndOnClick {
                changeEmail()
            }
            fragment_student_parameters_change_password_button.setPushAndOnClick {
                changePassword()
            }
            fragment_student_parameters_logout_button.setPushAndOnClick {
                logout()
            }
            fragment_student_home_top_bar_delete_button.setPushAndOnClick {
                deleteAccount()
            }
        }
    }

}