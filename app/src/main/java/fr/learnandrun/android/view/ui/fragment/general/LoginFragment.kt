package fr.learnandrun.android.view.ui.fragment.general

import android.os.Bundle
import android.view.View
import android.view.inputmethod.EditorInfo
import fr.learnandrun.android.R
import fr.learnandrun.android.tools.setPushAndOnClick
import fr.learnandrun.android.view.ui.fragment.BasicFragment
import fr.learnandrun.android.viewmodel.general.LoginViewModel
import kotlinx.android.synthetic.main.fragment_login.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class LoginFragment : BasicFragment<LoginViewModel>(R.layout.fragment_login) {

    override val viewModel: LoginViewModel by viewModel()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.apply {
            // Bind id to edit text corresponding
            idLiveText.bindEditTextView(viewLifecycleOwner, fragment_login_id_field.editText)

            // Bind password to edit text corresponding
            passwordLiveText.bindEditTextView(
                viewLifecycleOwner,
                fragment_login_password_field.editText
            )
            connectButtonIsActiveLive.observe(viewLifecycleOwner) {
                fragment_login_connection_button.setActive(it)
            }
            fragment_login_password_forgotten_text_view.setPushAndOnClick {
                passwordForgotten()
            }
            fragment_login_subscribe_text_view.setPushAndOnClick {
                signUp()
            }
            // Trigger connection with the Done button on the keyboard
            fragment_login_password_field.editText.setOnEditorActionListener { _, actionId, _ ->
                when (actionId) {
                    EditorInfo.IME_ACTION_SEARCH, EditorInfo.IME_ACTION_DONE -> {
                        connect()
                        true
                    }
                    else -> false
                }
            }
            fragment_login_connection_button.setPushAndOnClick {
                connect()
            }
        }

    }


}