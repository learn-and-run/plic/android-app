package fr.learnandrun.android.view.ui.element

import android.content.Context
import android.content.res.ColorStateList
import android.content.res.TypedArray
import android.text.SpannableStringBuilder
import android.util.AttributeSet
import android.util.TypedValue
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.content.res.getStringOrThrow
import fr.learnandrun.android.R
import kotlinx.android.synthetic.main.button.view.*

class ButtonView(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    constructor(context: Context) : this(context, null, 0)
    constructor(context: Context, attrs: AttributeSet? = null) : this(context, attrs, 0)

    val textView: TextView
        get() = button_text_view

    val pictoImageView: ImageView
        get() = button_image_view

    var activatedColors: ColorSet
    var deactivatedColors: ColorSet

    init {
        inflate(context, R.layout.button, this)
        val attributes: TypedArray = context.obtainStyledAttributes(attrs, R.styleable.ButtonView)

        val pictoDrawable = attributes.getResourceId(R.styleable.ButtonView_pictoDrawable, -1)
        if (pictoDrawable != -1) {
            pictoImageView.setImageDrawable(
                ContextCompat.getDrawable(context, pictoDrawable)
            )
            pictoImageView.visibility = VISIBLE
        }

        textView.text = SpannableStringBuilder(
            attributes.getStringOrThrow(R.styleable.ButtonView_text)
        )
        textView.setTextSize(
            TypedValue.COMPLEX_UNIT_SP,
            attributes.getInteger(R.styleable.ButtonView_textSize, 20).toFloat()
        )
        activatedColors = ColorSet(
            attributes.getColor(
                R.styleable.ButtonView_colorBackground,
                ContextCompat.getColor(context, R.color.colorWhite)
            ),
            attributes.getColor(
                R.styleable.ButtonView_colorForeground,
                ContextCompat.getColor(context, R.color.colorBlack)
            )
        )
        deactivatedColors = ColorSet(
            attributes.getColor(
                R.styleable.ButtonView_colorBackgroundDeactivated,
                ContextCompat.getColor(context, R.color.colorGray)
            ),
            attributes.getColor(
                R.styleable.ButtonView_colorForegroundDeactivated,
                ContextCompat.getColor(context, R.color.colorGreen)
            )
        )
        setActive(attributes.getBoolean(R.styleable.ButtonView_active, true))
        attributes.recycle()
        textView.isClickable = false
        pictoImageView.isClickable = false
    }

    fun setActive(active: Boolean) {
        fun setColor(colorSet: ColorSet) {
            button_view.backgroundTintList = ColorStateList.valueOf(colorSet.background)
            textView.setTextColor(colorSet.foreground)
        }
        setColor(if (active) activatedColors else deactivatedColors)

        isClickable = active
        isEnabled = active
    }

}