package fr.learnandrun.android.view.ui.element

import android.content.Context
import android.content.res.ColorStateList
import android.content.res.TypedArray
import android.text.InputType
import android.text.SpannableStringBuilder
import android.util.AttributeSet
import android.util.TypedValue
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.ImageButton
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import fr.learnandrun.android.R
import fr.learnandrun.android.tools.setPushAndOnClick
import fr.learnandrun.android.view.ui.element.FieldView.TextType.Companion.toTextType
import kotlinx.android.synthetic.main.field.view.*

class FieldView(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    constructor(context: Context) : this(context, null, 0)
    constructor(context: Context, attrs: AttributeSet? = null) : this(context, attrs, 0)

    val editText: EditText
        get() = field_edit_text_view
    val eyeButton: ImageButton
        get() = field_password_eye_image_button

    var activatedColors: ColorSet
    var deactivatedColors: ColorSet

    init {
        inflate(context, R.layout.field, this)
        val attributes: TypedArray = context.obtainStyledAttributes(attrs, R.styleable.FieldView)

        editText.hint = attributes.getString(R.styleable.FieldView_hint) ?: ""
        editText.text = SpannableStringBuilder(
            attributes.getString(R.styleable.FieldView_text) ?: ""
        )
        editText.setTextSize(
            TypedValue.COMPLEX_UNIT_SP,
            attributes.getInteger(R.styleable.FieldView_textSize, 20).toFloat()
        )
        setTextType(
            attributes.getInt(
                R.styleable.FieldView_android_inputType,
                EditorInfo.TYPE_TEXT_VARIATION_NORMAL
            )
        )
        activatedColors = ColorSet(
            attributes.getColor(
                R.styleable.FieldView_colorBackground,
                ContextCompat.getColor(context, R.color.colorWhite)
            ),
            attributes.getColor(
                R.styleable.FieldView_colorForeground,
                ContextCompat.getColor(context, R.color.colorWhite)
            )
        )
        deactivatedColors = ColorSet(
            attributes.getColor(
                R.styleable.FieldView_colorBackgroundDeactivated,
                ContextCompat.getColor(context, R.color.colorGray)
            ),
            attributes.getColor(
                R.styleable.FieldView_colorForegroundDeactivated,
                ContextCompat.getColor(context, R.color.colorGray)
            )
        )
        setActive(attributes.getBoolean(R.styleable.FieldView_active, true))
        attributes.recycle()

        if (!isInEditMode) {
            eyeButton.setPushAndOnClick {
                toTextType(editText.inputType)?.invert()?.let {
                    setTextType(it.type)
                    editText.setSelection(editText.text.length)
                }
            }
        }
    }

    fun setTextType(value: Int) {
        val typeface = editText.typeface
        editText.inputType = InputType.TYPE_CLASS_TEXT.or(value)
        editText.typeface = typeface
        val textType = toTextType(InputType.TYPE_CLASS_TEXT.or(value))

        eyeButton.visibility =
            if (textType == null) View.GONE else View.VISIBLE
        textType?.let {
            eyeButton.setImageDrawable(
                ContextCompat.getDrawable(context, textType.imageId)
            )
        }
    }

    fun setActive(active: Boolean) {
        fun setColor(colorSet: ColorSet) {
            field_view.backgroundTintList = ColorStateList.valueOf(colorSet.background)
            editText.setTextColor(colorSet.foreground)
            eyeButton.imageTintList = ColorStateList.valueOf(colorSet.background)
        }
        setColor(if (active) activatedColors else deactivatedColors)

        editText.isEnabled = active
        editText.isClickable = active
        eyeButton.isClickable = active
    }


    enum class TextType(val type: Int, val imageId: Int) {
        PASSWORD_TYPE(
            InputType.TYPE_CLASS_TEXT.or(InputType.TYPE_TEXT_VARIATION_PASSWORD),
            R.drawable.eyelash
        ),
        PASSWORD_VISIBLE_TYPE(
            InputType.TYPE_CLASS_TEXT.or(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD),
            R.drawable.eye
        );

        fun invert() = if (this == PASSWORD_TYPE) PASSWORD_VISIBLE_TYPE else PASSWORD_TYPE

        companion object {
            fun toTextType(inputType: Int): TextType? = when (inputType) {
                InputType.TYPE_CLASS_TEXT.or(InputType.TYPE_TEXT_VARIATION_PASSWORD) ->
                    PASSWORD_TYPE
                InputType.TYPE_CLASS_TEXT.or(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD) ->
                    PASSWORD_VISIBLE_TYPE
                else -> null
            }
        }
    }

}