package fr.learnandrun.android.view.ui.fragment.general

import android.os.Bundle
import android.view.View
import fr.learnandrun.android.R
import fr.learnandrun.android.tools.setPushAndOnClick
import fr.learnandrun.android.view.ui.fragment.BasicFragment
import fr.learnandrun.android.viewmodel.general.ChooseCreateAccountViewModel
import kotlinx.android.synthetic.main.fragment_choose_create_account.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class ChooseCreateAccountFragment
    : BasicFragment<ChooseCreateAccountViewModel>(R.layout.fragment_choose_create_account) {

    override val viewModel: ChooseCreateAccountViewModel by viewModel()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        fragment_choose_create_account_student_button.setPushAndOnClick {
            viewModel.chooseStudent()
        }

        fragment_choose_create_account_parent_button.setPushAndOnClick {
            viewModel.chooseParent()
        }
    }
}