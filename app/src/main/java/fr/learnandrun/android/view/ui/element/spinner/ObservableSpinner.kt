package fr.learnandrun.android.view.ui.element.spinner

import android.content.Context
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatSpinner
import fr.learnandrun.android.tools.delegate.DelegateObservable

class ObservableSpinner(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : AppCompatSpinner(context, attrs, defStyleAttr) {

    constructor(context: Context) : this(context, null, 0)
    constructor(context: Context, attrs: AttributeSet? = null) : this(context, attrs, 0)

    val isOpenObservable = DelegateObservable(false)
    var isOpen by isOpenObservable

    override fun performClick(): Boolean {
        isOpen = !isOpen
        return super.performClick()
    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)
        if (isOpen && hasFocus)
            isOpen = false
    }

    public override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
    }


}