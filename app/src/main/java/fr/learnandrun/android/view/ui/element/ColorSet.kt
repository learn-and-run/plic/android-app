package fr.learnandrun.android.view.ui.element

data class ColorSet(
    val background: Int,
    val foreground: Int
)