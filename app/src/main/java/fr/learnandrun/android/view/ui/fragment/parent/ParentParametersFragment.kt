package fr.learnandrun.android.view.ui.fragment.parent

import android.os.Bundle
import android.view.View
import fr.learnandrun.android.R
import fr.learnandrun.android.tools.setPushAndOnClick
import fr.learnandrun.android.view.ui.fragment.BasicFragment
import fr.learnandrun.android.viewmodel.parent.ParentParametersViewModel
import kotlinx.android.synthetic.main.fragment_parent_parameters.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class ParentParametersFragment : BasicFragment<ParentParametersViewModel>(R.layout.fragment_parent_parameters) {

    override val viewModel: ParentParametersViewModel by viewModel()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.apply {
            firstNameFieldLive.bindEditTextView(
                viewLifecycleOwner,
                fragment_parent_parameters_firstname_field.editText
            )
            lastNameFieldLive.bindEditTextView(
                viewLifecycleOwner,
                fragment_parent_parameters_lastname_field.editText
            )
            pseudoFieldLive.bindEditTextView(
                viewLifecycleOwner,
                fragment_parent_parameters_pseudo_field.editText
            )
            validateIsActiveLive.observe(viewLifecycleOwner) {
                fragment_parent_parameters_validate_button.setActive(it)
            }
            fragment_parent_parameters_validate_button.setPushAndOnClick {
                validate()
            }
            fragment_parent_parameters_top_bar_back_button.setPushAndOnClick {
                back()
            }
            fragment_parent_parameters_change_email_button.setPushAndOnClick {
                changeEmail()
            }
            fragment_parent_parameters_change_password_button.setPushAndOnClick {
                changePassword()
            }
            fragment_parent_parameters_logout_button.setPushAndOnClick {
                logout()
            }
            fragment_parent_home_top_bar_delete_button.setPushAndOnClick {
                deleteAccount()
            }
        }
    }

}