package fr.learnandrun.android.view.ui.fragment.general

import android.content.res.ColorStateList
import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.lifecycle.viewModelScope
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import fr.learnandrun.android.R
import fr.learnandrun.android.service.model.user.ModuleType
import fr.learnandrun.android.tools.setPushAndOnClick
import fr.learnandrun.android.view.adapter.ListArrayAdapter
import fr.learnandrun.android.view.adapter.SpinnerArrayAdapter
import fr.learnandrun.android.view.ui.element.decoration.SimpleDividerItemDecoration
import fr.learnandrun.android.view.ui.fragment.BasicFragment
import fr.learnandrun.android.viewmodel.general.StatsStudentViewModel
import kotlinx.android.synthetic.main.fragment_stats_students.*
import kotlinx.android.synthetic.main.student_score_circuit_row.view.*
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel

class StatsStudentFragment : BasicFragment<StatsStudentViewModel>(R.layout.fragment_stats_students) {

    override val viewModel: StatsStudentViewModel by viewModel()
    val args: StatsStudentFragmentArgs by navArgs()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.apply {
            isRefreshingLive.observe(viewLifecycleOwner) {
                fragment_stats_student_swipe_list_refresh_layout.isRefreshing = it
                if (it) {
                    fragment_stats_student_empty_list_text_view.visibility = View.GONE
                } else {
                    if (listScores?.isEmpty() == true) {
                        fragment_stats_student_empty_list_text_view.visibility = View.VISIBLE
                        fragment_stats_student_list_view.visibility = View.GONE
                    } else {
                        fragment_stats_student_empty_list_text_view.visibility = View.GONE
                        fragment_stats_student_list_view.visibility = View.VISIBLE
                    }
                }
            }
            fragment_stats_student_swipe_list_refresh_layout.setOnRefreshListener {
                viewModelScope.launch {
                    refreshList()
                }
            }
            fragment_stats_student_top_bar_back_button.setPushAndOnClick {
                back()
            }
            pseudoLive.observe(viewLifecycleOwner) {
                viewModelScope.launch {
                    moduleClick(selectedModule)
                }
                fragment_stats_student_top_bar_student_name.text = it
            }
            fragment_stats_student_classe_spinner_view.apply spinnerView@{
                openedColorIsClosedColor = true
                observableSpinner.apply observableSpinner@{
                    setPopupBackgroundDrawable(
                        ContextCompat.getDrawable(requireContext(), R.drawable.rectangle_rounded)
                    )
                    adapter = SpinnerArrayAdapter(
                        requireContext(),
                        this@spinnerView,
                        modules,
                        ModuleType::displayName,
                        getBackColor = ModuleType::color,
                        getForeColor = { R.color.colorWhite }
                    ) {
                        closedColor = ContextCompat.getColor(requireContext(), it.color)
                        this@spinnerView.setActive(true)
                        viewModelScope.launch {
                            moduleClick(it)
                        }
                    }.also { it.select(0) }
                    dropDownVerticalOffset = 20.dp()
                }
            }
            fragment_stats_student_list_view.apply {
                addItemDecoration(
                    SimpleDividerItemDecoration(
                        ContextCompat.getColor(requireContext(), R.color.colorGreen),
                        2
                    )
                )
                setHasFixedSize(true)
                layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
            }
            class CircuitScoreViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
                val dotView = itemView.student_circuit_score_row_dot_view
                val circuitTextView = itemView.student_circuit_score_row_circuit_text
                val percentTextView = itemView.student_circuit_score_row_percent_text
            }
            listScoresLive.observe(viewLifecycleOwner) { listScores ->
                listScores?.let {
                    fragment_stats_student_list_view.adapter = ListArrayAdapter(
                        R.layout.student_score_circuit_row,
                        { CircuitScoreViewHolder(it) },
                        { holder, scoreModel ->
                            holder.dotView.backgroundTintList = ColorStateList.valueOf(
                                ContextCompat.getColor(requireContext(), selectedModule.color)
                            )
                            holder.circuitTextView.setTextColor(
                                ContextCompat.getColor(requireContext(), selectedModule.color)
                            )
                            holder.circuitTextView.text = scoreModel.circuitName
                            holder.percentTextView.setTextColor(
                                ContextCompat.getColor(requireContext(), selectedModule.color)
                            )
                            holder.percentTextView.text = getString(R.string.percent_value, scoreModel.score)
                        },
                        listScores
                    )
                }
            }
            pseudo = args.pseudo
        }

    }

    fun Int.dp(): Int {
        val scale = requireContext().resources.displayMetrics.density
        val pixels = this * scale + 0.5f
        return pixels.toInt()
    }

}