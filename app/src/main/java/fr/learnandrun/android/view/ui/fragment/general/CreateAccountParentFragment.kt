package fr.learnandrun.android.view.ui.fragment.general

import android.os.Bundle
import android.view.View
import fr.learnandrun.android.R
import fr.learnandrun.android.tools.setPushAndOnClick
import fr.learnandrun.android.view.ui.fragment.BasicFragment
import fr.learnandrun.android.viewmodel.general.CreateAccountParentViewModel
import kotlinx.android.synthetic.main.fragment_create_account_parent.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class CreateAccountParentFragment
    : BasicFragment<CreateAccountParentViewModel>(R.layout.fragment_create_account_parent) {

    override val viewModel: CreateAccountParentViewModel by viewModel()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.apply {

            emailLiveText.bindEditTextView(
                viewLifecycleOwner,
                fragment_create_account_parent_email_field_view.editText
            )

            lastnameLiveText.bindEditTextView(
                viewLifecycleOwner,
                fragment_create_account_parent_lastname_field_view.editText
            )

            firstnameLiveText.bindEditTextView(
                viewLifecycleOwner,
                fragment_create_account_parent_firstname_field_view.editText
            )

            pseudoLiveText.bindEditTextView(
                viewLifecycleOwner,
                fragment_create_account_parent_pseudo_field_view.editText
            )

            passwordLiveText.bindEditTextView(
                viewLifecycleOwner,
                fragment_create_account_parent_password_field_view.editText
            )

            createIsActiveLive.observe(viewLifecycleOwner) {
                fragment_create_account_parent_create_button_view.setActive(it)
            }

            fragment_create_account_parent_create_button_view.setPushAndOnClick {
                create()
            }
            fragment_create_account_parent_back_button_view.setPushAndOnClick {
                back()
            }
        }
    }
}