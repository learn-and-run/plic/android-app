package fr.learnandrun.android.view.ui.element.spinner

import android.content.Context
import android.content.res.ColorStateList
import android.content.res.TypedArray
import android.text.SpannableStringBuilder
import android.util.AttributeSet
import android.util.TypedValue
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.content.res.getStringOrThrow
import fr.learnandrun.android.R
import fr.learnandrun.android.tools.setPushAndOnClick
import kotlinx.android.synthetic.main.spinner.view.*

class SpinnerView(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    constructor(context: Context) : this(context, null, 0)
    constructor(context: Context, attrs: AttributeSet? = null) : this(context, attrs, 0)

    var openedColorIsClosedColor = false

    val textView: TextView
        get() = spinner_text_view

    val imageView: ImageView
        get() = spinner_image_view

    val observableSpinner: ObservableSpinner
        get() = spinner_spinner_view

    val innerContainer: ConstraintLayout
        get() = spinner_inner_container

    var closedColor: Int
    var closedBorderColor: Int
    var closedTextColor: Int
    var deactivatedColor: Int

    init {
        inflate(context, R.layout.spinner, this)
        val attributes: TypedArray = context.obtainStyledAttributes(attrs, R.styleable.SpinnerView)

        textView.text = SpannableStringBuilder(
            attributes.getStringOrThrow(R.styleable.SpinnerView_text)
        )

        textView.setTextSize(
            TypedValue.COMPLEX_UNIT_SP,
            attributes.getInteger(R.styleable.SpinnerView_textSize, 20).toFloat()
        )

        val openedColor = attributes.getColor(
            R.styleable.SpinnerView_openedColor,
            ContextCompat.getColor(context, R.color.colorYellow)
        )

        val openedBorderColor = attributes.getColor(
            R.styleable.SpinnerView_openedBorderColor,
            ContextCompat.getColor(context, R.color.colorYellow)
        )

        val openedTextColor = attributes.getColor(
            R.styleable.SpinnerView_openedTextColor,
            ContextCompat.getColor(context, R.color.colorGreen)
        )

        closedColor = attributes.getColor(
            R.styleable.SpinnerView_closedColor,
            ContextCompat.getColor(context, R.color.colorGreen)
        )

        closedBorderColor = attributes.getColor(
            R.styleable.SpinnerView_closedBorderColor,
            ContextCompat.getColor(context, R.color.colorWhite)
        )

        closedTextColor = attributes.getColor(
            R.styleable.SpinnerView_closedTextColor,
            ContextCompat.getColor(context, R.color.colorWhite)
        )

        val openedRotation = attributes.getFloat(
            R.styleable.SpinnerView_openedRotation,
            90F
        )

        val closedRotation = attributes.getFloat(
            R.styleable.SpinnerView_closedRotation,
            90F
        )

        deactivatedColor = attributes.getColor(
            R.styleable.SpinnerView_deactivatedColor,
            ContextCompat.getColor(context, R.color.colorGray)
        )

        imageView.rotation = closedRotation
        innerContainer.backgroundTintList = ColorStateList.valueOf(closedColor)

        attributes.recycle()
        textView.isClickable = false
        imageView.isClickable = false

        if (!isInEditMode) {
            observableSpinner.isOpenObservable.observer = { isOpen ->
                if (isOpen) {
                    imageView.rotation = openedRotation
                    if (!openedColorIsClosedColor) {
                        spinner.backgroundTintList = ColorStateList.valueOf(openedBorderColor)
                        innerContainer.backgroundTintList = ColorStateList.valueOf(openedColor)
                        textView.setTextColor(openedTextColor)
                        imageView.imageTintList = ColorStateList.valueOf(openedTextColor)
                    }
                } else {
                    imageView.rotation = closedRotation
                    spinner.backgroundTintList = ColorStateList.valueOf(closedBorderColor)
                    innerContainer.backgroundTintList = ColorStateList.valueOf(closedColor)
                    textView.setTextColor(closedTextColor)
                    imageView.imageTintList = ColorStateList.valueOf(closedTextColor)
                }
            }

            setPushAndOnClick {
                observableSpinner.performClick()
            }
        }

        setActive(true)
    }

    fun setActive(active: Boolean) {
        isClickable = active
        isEnabled = active
        if (active) {
            spinner.backgroundTintList = ColorStateList.valueOf(closedBorderColor)
            textView.setTextColor(closedTextColor)
            imageView.imageTintList = ColorStateList.valueOf(closedTextColor)
        } else {
            spinner.backgroundTintList = ColorStateList.valueOf(deactivatedColor)
            textView.setTextColor(deactivatedColor)
            imageView.imageTintList = ColorStateList.valueOf(deactivatedColor)
        }
    }

}