package fr.learnandrun.android.view.ui.fragment.parent

import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.lifecycle.viewModelScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import fr.learnandrun.android.R
import fr.learnandrun.android.service.model.user.StudentModel
import fr.learnandrun.android.tools.setPushAndOnClick
import fr.learnandrun.android.view.adapter.ListArrayAdapter
import fr.learnandrun.android.view.ui.element.decoration.SimpleDividerItemDecoration
import fr.learnandrun.android.view.ui.fragment.BasicFragment
import fr.learnandrun.android.viewmodel.parent.ParentHomeViewModel
import kotlinx.android.synthetic.main.fragment_parent_home.*
import kotlinx.android.synthetic.main.person_row.view.*
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel

class ParentHomeFragment : BasicFragment<ParentHomeViewModel>(R.layout.fragment_parent_home) {

    override val viewModel: ParentHomeViewModel by viewModel()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.apply {
            isRefreshingLive.observe(viewLifecycleOwner) {
                fragment_parent_home_swipe_list_refresh_layout.isRefreshing = it
                if (it)
                    fragment_parent_home_empty_list_text_view.visibility = View.GONE
                else {
                    if (listStudentRelations?.isEmpty() == true) {
                        fragment_parent_home_empty_list_text_view.visibility = View.VISIBLE
                        fragment_parent_home_list_view.visibility = View.GONE
                    } else {
                        fragment_parent_home_empty_list_text_view.visibility = View.GONE
                        fragment_parent_home_list_view.visibility = View.VISIBLE
                    }
                }
            }
            fragment_parent_home_swipe_list_refresh_layout.setOnRefreshListener {
                viewModelScope.launch {
                    refreshList()
                }
            }
            fragment_parent_home_top_bar_settings_button.setPushAndOnClick {
                settings()
            }
            fragment_parent_home_top_bar_invitations_button.setPushAndOnClick {
                invitations()
            }
            fragment_parent_home_add_child_button.setPushAndOnClick {
                addStudent()
            }

            class StudentRowViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
                val rowLayout = itemView.person_row_layout
                val trashButton = itemView.person_row_swipe_trash_button
                val studentPseudoTextView = itemView.person_row_name_text
                val studentClasseTextView = itemView.person_row_info_text
            }

            fragment_parent_home_list_view.apply {
                addItemDecoration(
                    SimpleDividerItemDecoration(
                        ContextCompat.getColor(requireContext(), R.color.colorGreen),
                        2
                    )
                )
                setHasFixedSize(true)
                layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
            }
            listStudentRelationsLive.observe(viewLifecycleOwner) { listRelations ->
                listRelations?.let {
                    fragment_parent_home_list_view.adapter = ListArrayAdapter(
                        R.layout.person_row,
                        { StudentRowViewHolder(it) },
                        { holder, studentRelation ->
                            val studentModel = studentRelation.userModel as StudentModel
                            holder.rowLayout.setPushAndOnClick {
                                studentClick(studentModel)
                            }
                            holder.trashButton.setPushAndOnClick {
                                studentTrash(studentRelation)
                            }
                            holder.studentPseudoTextView.text = studentModel.pseudo
                            holder.studentClasseTextView.text = studentModel.levelType.displayName
                        },
                        listRelations
                    )
                }
            }
        }
    }

}