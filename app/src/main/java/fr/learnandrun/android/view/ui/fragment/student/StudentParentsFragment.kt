package fr.learnandrun.android.view.ui.fragment.student

import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.lifecycle.viewModelScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import fr.learnandrun.android.R
import fr.learnandrun.android.service.model.user.ParentModel
import fr.learnandrun.android.tools.setPushAndOnClick
import fr.learnandrun.android.view.adapter.ListArrayAdapter
import fr.learnandrun.android.view.ui.element.decoration.SimpleDividerItemDecoration
import fr.learnandrun.android.view.ui.fragment.BasicFragment
import fr.learnandrun.android.viewmodel.student.StudentParentsViewModel
import kotlinx.android.synthetic.main.fragment_student_parents.*
import kotlinx.android.synthetic.main.person_row.view.*
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel

class StudentParentsFragment : BasicFragment<StudentParentsViewModel>(R.layout.fragment_student_parents) {

    override val viewModel: StudentParentsViewModel by viewModel()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        viewModel.apply {
            refreshingLive.observe(viewLifecycleOwner) {
                fragment_student_parent_swipe_list_refresh_layout.isRefreshing = it
                if (it)
                    fragment_student_parent_empty_list_text_view.visibility = View.GONE
                else {
                    if (listParentRelations?.isEmpty() == true) {
                        fragment_student_parent_list_view.visibility = View.GONE
                        fragment_student_parent_empty_list_text_view.visibility = View.VISIBLE
                    } else {
                        fragment_student_parent_list_view.visibility = View.VISIBLE
                        fragment_student_parent_empty_list_text_view.visibility = View.GONE
                    }
                }
            }
            fragment_student_parent_add_parent_button.setPushAndOnClick {
                addParent()
            }
            fragment_student_parent_swipe_list_refresh_layout.setOnRefreshListener {
                viewModelScope.launch {
                    refreshParents()
                }
            }
            fragment_student_parent_top_bar_back_button.setPushAndOnClick {
                back()
            }
            fragment_student_parent_list_view.apply {
                addItemDecoration(
                    SimpleDividerItemDecoration(
                        ContextCompat.getColor(requireContext(), R.color.colorGreen),
                        2
                    )
                )
                setHasFixedSize(true)
                layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
            }
            class ParentViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
                val rowLayout = itemView.person_row_layout
                val trashButton = itemView.person_row_swipe_trash_button
                val parentPseudoTextView = itemView.person_row_name_text
                val infoTextView = itemView.person_row_info_text
            }
            listParentRelationsLive.observe(viewLifecycleOwner) { listParentRelations ->
                listParentRelations ?: return@observe
                fragment_student_parent_list_view.adapter = ListArrayAdapter(
                    R.layout.person_row,
                    { ParentViewHolder(it) },
                    { holder, parentRelation ->
                        val parentModel = parentRelation.userModel as ParentModel
                        holder.trashButton.setPushAndOnClick {
                            parentTrash(parentRelation)
                        }
                        holder.parentPseudoTextView.text = parentModel.pseudo
                        holder.infoTextView.visibility = View.GONE
                    },
                    listParentRelations
                )
            }
        }
    }

}