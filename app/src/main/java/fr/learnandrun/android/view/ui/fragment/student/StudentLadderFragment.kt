package fr.learnandrun.android.view.ui.fragment.student

import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.lifecycle.viewModelScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import fr.learnandrun.android.R
import fr.learnandrun.android.service.model.user.ModuleType
import fr.learnandrun.android.tools.setPushAndOnClick
import fr.learnandrun.android.view.adapter.ListArrayAdapter
import fr.learnandrun.android.view.adapter.SpinnerArrayAdapter
import fr.learnandrun.android.view.ui.element.decoration.SimpleDividerItemDecoration
import fr.learnandrun.android.view.ui.fragment.BasicFragment
import fr.learnandrun.android.viewmodel.student.StudentLadderViewModel
import kotlinx.android.synthetic.main.fragment_student_ladder.*
import kotlinx.android.synthetic.main.student_ladder_row.view.*
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel

class StudentLadderFragment : BasicFragment<StudentLadderViewModel>(R.layout.fragment_student_ladder) {

    override val viewModel: StudentLadderViewModel by viewModel()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.apply {
            isRefreshingLive.observe(viewLifecycleOwner) {
                fragment_student_ladder_swipe_list_refresh_layout.isRefreshing = it
                if (it) {
                    fragment_student_ladder_empty_list_text_view.visibility = View.GONE
                } else {
                    if (listLadder?.isEmpty() == true) {
                        fragment_student_ladder_empty_list_text_view.visibility = View.VISIBLE
                        fragment_student_ladder_list_view.visibility = View.GONE
                    } else {
                        fragment_student_ladder_empty_list_text_view.visibility = View.GONE
                        fragment_student_ladder_list_view.visibility = View.VISIBLE
                    }
                }
            }
            fragment_student_ladder_swipe_list_refresh_layout.setOnRefreshListener {
                viewModelScope.launch {
                    refreshList()
                }
            }
            fragment_student_ladder_top_bar_back_button.setPushAndOnClick {
                back()
            }
            fragment_student_ladder_classe_spinner_view.apply spinnerView@{
                openedColorIsClosedColor = true
                observableSpinner.apply observableSpinner@{
                    setPopupBackgroundDrawable(
                        ContextCompat.getDrawable(requireContext(), R.drawable.rectangle_rounded)
                    )
                    adapter = SpinnerArrayAdapter(
                        requireContext(),
                        this@spinnerView,
                        modules,
                        ModuleType::displayName,
                        getBackColor = ModuleType::color,
                        getForeColor = { R.color.colorWhite }
                    ) {
                        closedColor = ContextCompat.getColor(requireContext(), it.color)
                        this@spinnerView.setActive(true)
                        viewModelScope.launch {
                            moduleClick(it)
                        }
                    }.also { it.select(0) }
                    dropDownVerticalOffset = 20.dp()
                }
            }
            fragment_student_ladder_list_view.apply {
                addItemDecoration(
                    SimpleDividerItemDecoration(
                        ContextCompat.getColor(requireContext(), R.color.colorGreen),
                        2
                    )
                )
                setHasFixedSize(true)
                layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
            }
            class LadderItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
                val rankTextView = itemView.student_ladder_row_rank
                val pseudoTextView = itemView.student_ladder_row_pseudo
                val scoreTextView = itemView.student_ladder_row_score
            }
            listLadderLive.observe(viewLifecycleOwner) { ladderItems ->
                ladderItems?.let {
                    fragment_student_ladder_list_view.adapter = ListArrayAdapter(
                        R.layout.student_ladder_row,
                        { LadderItemViewHolder(it) },
                        { holder, ladderModel ->
                            holder.rankTextView.text = ladderModel.ladder.toString()
                            holder.pseudoTextView.text = ladderModel.pseudo
                            holder.scoreTextView.text = getString(R.string.percent_value, ladderModel.score)
                        },
                        ladderItems
                    )
                }
            }
        }

    }

    fun Int.dp(): Int {
        val scale = requireContext().resources.displayMetrics.density
        val pixels = this * scale + 0.5f
        return pixels.toInt()
    }

}