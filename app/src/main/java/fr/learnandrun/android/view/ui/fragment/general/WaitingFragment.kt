package fr.learnandrun.android.view.ui.fragment.general

import android.os.Bundle
import android.view.View
import fr.learnandrun.android.R
import fr.learnandrun.android.view.ui.fragment.BasicFragment
import fr.learnandrun.android.viewmodel.general.WaitingViewModel
import kotlinx.android.synthetic.main.fragment_waiting.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class WaitingFragment : BasicFragment<WaitingViewModel>(R.layout.fragment_waiting) {

    override val viewModel: WaitingViewModel by viewModel()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.apply {
            statusLive.observe(viewLifecycleOwner) { value ->
                value?.let {
                    fragment_waiting_status_text_view.text = it.getMessage(requireContext())
                }
            }
            start()
        }

    }

}