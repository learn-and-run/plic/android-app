package fr.learnandrun.android.view.ui.fragment.general

import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.lifecycle.viewModelScope
import fr.learnandrun.android.R
import fr.learnandrun.android.service.model.user.LevelType
import fr.learnandrun.android.tools.setPushAndOnClick
import fr.learnandrun.android.view.adapter.SpinnerArrayAdapter
import fr.learnandrun.android.view.ui.fragment.BasicFragment
import fr.learnandrun.android.viewmodel.general.CreateAccountStudentViewModel
import kotlinx.android.synthetic.main.fragment_create_account_student.*
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel

class CreateAccountStudentFragment
    : BasicFragment<CreateAccountStudentViewModel>(R.layout.fragment_create_account_student) {

    override val viewModel: CreateAccountStudentViewModel by viewModel()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        fragment_create_account_student_class_spinner_view.apply spinnerView@{
            observableSpinner.apply observableSpinner@{
                setPopupBackgroundDrawable(
                    ContextCompat.getDrawable(requireContext(), R.drawable.rectangle_rounded)
                )
                popupBackground.setTint(
                    ContextCompat.getColor(requireContext(), R.color.colorYellow)
                )
                adapter = SpinnerArrayAdapter(
                    requireContext(),
                    this@spinnerView,
                    LevelType.values().toList(),
                    LevelType::displayName,
                    getBackColor = { R.color.colorYellow },
                    getForeColor = { R.color.colorGreen }
                ) {
                    viewModel.classe = it
                }
                viewModel.viewModelScope.launch {
                    delay(1)
                    dropDownHorizontalOffset = fragment_create_account_student_class_spinner_view.measuredWidth
                }

                //dropDownVerticalOffset = (-47).dp()
            }
        }

        viewModel.apply {

            emailLiveText.bindEditTextView(
                viewLifecycleOwner,
                fragment_create_account_student_email_field_view.editText
            )
            lastnameLiveText.bindEditTextView(
                viewLifecycleOwner,
                fragment_create_account_student_lastname_field_view.editText
            )
            firstnameLiveText.bindEditTextView(
                viewLifecycleOwner,
                fragment_create_account_student_firstname_field_view.editText
            )
            pseudoLiveText.bindEditTextView(
                viewLifecycleOwner,
                fragment_create_account_student_pseudo_field_view.editText
            )
            passwordLiveText.bindEditTextView(
                viewLifecycleOwner,
                fragment_create_account_student_password_field_view.editText
            )

            createIsActiveLive.observe(viewLifecycleOwner) {
                fragment_create_account_student_create_button_view.setActive(it)
            }
            classeLive.observe(viewLifecycleOwner) {
                if (it != null)
                    fragment_create_account_student_class_spinner_view.textView.text = it.displayName
            }

            fragment_create_account_student_create_button_view.setPushAndOnClick {
                create()
            }

            fragment_create_account_student_back_button_view.setPushAndOnClick {
                back()
            }
        }
    }

    fun Int.dp(): Int {
        val scale = requireContext().resources.displayMetrics.density
        val pixels = this * scale + 0.5f
        return pixels.toInt()
    }
}