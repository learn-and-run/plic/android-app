package fr.learnandrun.android.service.model.score

class ScoresByModuleModel(
    val scoreModels: List<ScoreModel>
)