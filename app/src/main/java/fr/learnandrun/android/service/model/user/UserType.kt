package fr.learnandrun.android.service.model.user

enum class UserType {
    STUDENT,
    PARENT,
    PROFESSOR
}