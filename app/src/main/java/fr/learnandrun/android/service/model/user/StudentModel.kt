package fr.learnandrun.android.service.model.user

class StudentModel(
    pseudo: String,
    firstname: String,
    lastname: String,
    email: String,
    characterModel: CharacterModel,
    var levelType: LevelType
) : UserModel(
    pseudo,
    firstname,
    lastname,
    email,
    UserType.STUDENT,
    characterModel
)