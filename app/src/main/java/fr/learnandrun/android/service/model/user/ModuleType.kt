package fr.learnandrun.android.service.model.user

import fr.learnandrun.android.R

enum class ModuleType(
    val color: Int,
    val displayName: String
) {
    GENERAL(R.color.colorGeneral, "Général"),

    MATHS(R.color.colorMaths, "Mathématiques"),

    FRANCAIS(R.color.colorFrancais, "Français"),

    PHYSIQUE(R.color.colorPhysique, "Physique"),

    CHIMIE(R.color.colorChimie, "Chimie"),

    ANGLAIS(R.color.colorAnglais, "Anglais"),

    HISTOIRE(R.color.colorHistoire, "Histoire"),

    GEOGRAPHIE(R.color.colorGeographie, "Géographie")
}