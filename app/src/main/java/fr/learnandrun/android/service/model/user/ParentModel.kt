package fr.learnandrun.android.service.model.user

class ParentModel(
    pseudo: String,
    firstname: String,
    lastname: String,
    email: String,
    characterModel: CharacterModel
) : UserModel(
    pseudo,
    firstname,
    lastname,
    email,
    UserType.PARENT,
    characterModel
)