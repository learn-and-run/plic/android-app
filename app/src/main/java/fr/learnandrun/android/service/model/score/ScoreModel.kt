package fr.learnandrun.android.service.model.score

import fr.learnandrun.android.service.model.user.ModuleType

class ScoreModel(
    val circuitName: String,
    val moduleType: ModuleType,
    val score: Int,
    val spendTime: Int,
    val bestTime: Int
)