package fr.learnandrun.android.service.model.user

data class InvitationModel(
    val id: Long,
    val pseudo: String
)