package fr.learnandrun.android.service.model.user

data class LadderModel(
    val ladder: Int,
    val pseudo: String,
    val score: Int
)