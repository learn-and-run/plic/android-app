package fr.learnandrun.android.service

import fr.learnandrun.android.repository.user.UserRepository
import fr.learnandrun.android.repository.user.dto.request.*
import fr.learnandrun.android.repository.user.dto.response.*
import fr.learnandrun.android.service.model.user.*
import fr.learnandrun.android.tools.converter.toListInvitationModels
import fr.learnandrun.android.tools.converter.toRelationModels
import fr.learnandrun.android.tools.converter.toUser
import fr.learnandrun.android.tools.handler.ApiCallHandler.Companion.handleFunctionCall
import fr.learnandrun.android.tools.handler.SetupApiCall

class UserService(
    private val userRepository: UserRepository
) : Service {

    suspend fun logout(
        setupApiCall: SetupApiCall<Unit>
    ) = handleFunctionCall(setupApiCall) {
        userRepository.logout()
    }

    suspend fun delete(
        setupApiCall: SetupApiCall<Unit>
    ) = handleFunctionCall(setupApiCall) {
        userRepository.delete()
    }

    suspend fun isAuth(
        setupApiCall: SetupApiCall<Boolean>
    ) = handleFunctionCall(setupApiCall) {
        userRepository.isAuth().isAuthenticated
    }

    suspend fun isFullyAuth(
        setupApiCall: SetupApiCall<Boolean>
    ) = handleFunctionCall(setupApiCall) {
        userRepository.isFullyAuth().isFullyAuthenticated
    }

    suspend fun login(
        username: String,
        password: String,
        rememberMe: Boolean,
        setupApiCall: SetupApiCall<Unit>
    ) = handleFunctionCall(setupApiCall) {
        userRepository.login(UserLoginDtoRequest(username, password, rememberMe))
    }

    suspend fun getInfo(
        setupApiCall: SetupApiCall<UserModel>
    ) = handleFunctionCall(setupApiCall) {
        userRepository.getInfo().user.toUser()
    }

    suspend fun createStudent(
        pseudo: String,
        firstname: String,
        lastname: String,
        password: String,
        email: String,
        levelType: LevelType,
        setupApiCall: SetupApiCall<CreateStudentDtoResponse>
    ) = handleFunctionCall(setupApiCall) {
        userRepository.createStudent(
            CreateStudentDtoRequest(pseudo, firstname, lastname, password, email, levelType)
        )
    }

    suspend fun createParent(
        pseudo: String,
        firstname: String,
        lastname: String,
        password: String,
        email: String,
        setupApiCall: SetupApiCall<CreateParentDtoResponse>
    ) = handleFunctionCall(setupApiCall) {
        userRepository.createParent(
            CreateParentDtoRequest(pseudo, firstname, lastname, password, email)
        )
    }

    suspend fun createProfessor(
        pseudo: String,
        firstname: String,
        lastname: String,
        password: String,
        email: String,
        setupApiCall: SetupApiCall<CreateProfessorDtoResponse>
    ) = handleFunctionCall(setupApiCall) {
        userRepository.createProfessor(
            CreateProfessorDtoRequest(pseudo, firstname, lastname, password, email)
        )
    }

    suspend fun updateStudent(
        pseudo: String,
        firstname: String,
        lastname: String,
        levelType: LevelType,
        setupApiCall: SetupApiCall<UpdateStudentDtoResponse>
    ) = handleFunctionCall(setupApiCall) {
        userRepository.updateStudent(
            UpdateStudentDtoRequest(pseudo, firstname, lastname, levelType)
        )
    }

    suspend fun updateParent(
        pseudo: String,
        firstname: String,
        lastname: String,
        setupApiCall: SetupApiCall<UpdateParentDtoResponse>
    ) = handleFunctionCall(setupApiCall) {
        userRepository.updateParent(UpdateParentDtoRequest(pseudo, firstname, lastname))
    }

    suspend fun updateProfessor(
        pseudo: String,
        firstname: String,
        lastname: String,
        setupApiCall: SetupApiCall<UpdateProfessorDtoResponse>
    ) = handleFunctionCall(setupApiCall) {
        userRepository.updateProfessor(UpdateProfessorDtoRequest(pseudo, firstname, lastname))
    }

    suspend fun updateEmail(
        email: String,
        setupApiCall: SetupApiCall<Unit>
    ) = handleFunctionCall(setupApiCall) {
        userRepository.updateEmail(UpdateEmailDtoRequest(email))
    }

    suspend fun updatePassword(
        password: String,
        setupApiCall: SetupApiCall<Unit>
    ) = handleFunctionCall(setupApiCall) {
        userRepository.updatePassword(UpdatePasswordDtoRequest(password))
    }

    suspend fun askResetPassword(
        email: String,
        setupApiCall: SetupApiCall<Unit>
    ) = handleFunctionCall(setupApiCall) {
        userRepository.askResetPassword(AskResetPasswordDtoRequest(email))
    }

    suspend fun getAllRelations(
        userType: UserType,
        setupApiCall: SetupApiCall<List<RelationModel>>
    ) = handleFunctionCall(setupApiCall) {
        userRepository.getAllRelations(AllRelationsDtoRequest(userType)).toRelationModels()
    }

    suspend fun addRelation(
        username: String,
        setupApiCall: SetupApiCall<Unit>
    ) = handleFunctionCall(setupApiCall) {
        userRepository.addRelation(AddRelationDtoRequest(username))
    }

    suspend fun validateRelation(
        relationId: Long,
        setupApiCall: SetupApiCall<Unit>
    ) = handleFunctionCall(setupApiCall) {
        userRepository.validateRelation(ValidateRelationRequest(relationId))
    }

    suspend fun getPendingRelations(
        setupApiCall: SetupApiCall<List<InvitationModel>>
    ) = handleFunctionCall(setupApiCall) {
        userRepository.getPendingRelations().toListInvitationModels()
    }

    suspend fun declineInvitation(
        invitationId: Long,
        setupApiCall: SetupApiCall<Unit>
    ) = handleFunctionCall(setupApiCall) {
        userRepository.declineInvitation(DeclineInvitationDtoRequest(invitationId))
    }

    suspend fun deleteRelation(
        relationId: Long,
        setupApiCall: SetupApiCall<Unit>
    ) = handleFunctionCall(setupApiCall) {
        userRepository.deleteRelation(DeleteRelationDtoRequest(relationId))
    }

}
