package fr.learnandrun.android.service.model.user

class CharacterModel(
    var id: Long,
    var name: String
)