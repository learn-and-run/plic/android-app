package fr.learnandrun.android.service

import fr.learnandrun.android.repository.score.ScoreRepository
import fr.learnandrun.android.repository.score.dto.request.CircuitScoreDtoRequest
import fr.learnandrun.android.repository.score.dto.request.RankingDtoRequest
import fr.learnandrun.android.repository.score.dto.request.ScoresOfPseudoByModuleDtoRequest
import fr.learnandrun.android.service.model.score.CircuitScoreModel
import fr.learnandrun.android.service.model.score.ScoresByModuleModel
import fr.learnandrun.android.service.model.user.LadderModel
import fr.learnandrun.android.service.model.user.ModuleType
import fr.learnandrun.android.tools.converter.toCircuitScore
import fr.learnandrun.android.tools.converter.toLadderList
import fr.learnandrun.android.tools.converter.toScoresByModule
import fr.learnandrun.android.tools.handler.ApiCallHandler.Companion.handleFunctionCall
import fr.learnandrun.android.tools.handler.SetupApiCall

class ScoreService(
    private val scoreRepository: ScoreRepository
) : Service {

    suspend fun getScoresOfPseudoByModule(
        pseudo: String,
        moduleType: ModuleType,
        setupApiCall: SetupApiCall<ScoresByModuleModel>
    ) = handleFunctionCall(setupApiCall) {
        scoreRepository.getScoresOfPseudoByModule(
            ScoresOfPseudoByModuleDtoRequest(pseudo, moduleType)
        ).toScoresByModule()
    }

    suspend fun getCircuitScore(
        pseudo: String,
        circuitName: String,
        setupApiCall: SetupApiCall<CircuitScoreModel>
    ) = handleFunctionCall(setupApiCall) {
        scoreRepository.getCircuitScore(
            CircuitScoreDtoRequest(pseudo, circuitName)
        ).toCircuitScore()
    }

    suspend fun getFriendsScoreByModule(
        moduleType: ModuleType,
        setupApiCall: SetupApiCall<List<LadderModel>>
    ) = handleFunctionCall(setupApiCall) {
        scoreRepository.getFriendsScoreByModule(
            RankingDtoRequest(moduleType)
        ).toLadderList()
    }

}
