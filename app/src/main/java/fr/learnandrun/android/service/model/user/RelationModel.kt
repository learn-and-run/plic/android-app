package fr.learnandrun.android.service.model.user

data class RelationModel(
    val id: Long,
    val userModel: UserModel
)