package fr.learnandrun.android.service.error

import kotlinx.serialization.Serializable

@Serializable
data class ApiCallErrorResponse(
    val timestamp: String,
    val status: Int,
    val error: String,
    val message: String,
    val path: String
)