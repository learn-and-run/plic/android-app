package fr.learnandrun.android.service.model.score

data class CircuitScoreModel(
    val circuitId: Long,
    val bestScore: Int,
    val currentScore: Int,
    val spendTime: Int,
    val bestTime: Int
)