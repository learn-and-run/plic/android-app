package fr.learnandrun.android.service.model.user

abstract class UserModel(
    var pseudo: String,
    var firstname: String,
    var lastname: String,
    var email: String,
    var userType: UserType,
    var characterModel: CharacterModel
)