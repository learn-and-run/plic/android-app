package fr.learnandrun.android.service.error

import android.util.Log
import io.ktor.client.features.*
import io.ktor.http.*

class ApiCallErrorException(
    val responseCode: HttpStatusCode?,
    val apiCallErrorResponse: ApiCallErrorResponse?,
    innerThrowable: Throwable
) : Exception(
    apiCallErrorResponse?.message ?: "No error message",
    innerThrowable
) {
    init {
        Log.d("ApiCallError", errorAsString())
        Log.d("ApiCallError", "Inner throwable:" + innerThrowable.message)
        Log.d("ApiCallError", innerThrowable.stackTraceToString())
    }

    fun errorAsString(): String =
        if (responseCode == null)
            "$message\n${stackTraceToString()}"
        else
            "[$responseCode] $message\n$apiCallErrorResponse\n${stackTraceToString()}"

    fun isTimeout() = when (cause) {
        is HttpRequestTimeoutException -> true
        else -> false
    }
}