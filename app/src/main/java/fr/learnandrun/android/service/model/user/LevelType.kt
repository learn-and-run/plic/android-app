package fr.learnandrun.android.service.model.user

enum class LevelType(
    val displayName: String
) {
    SIXIEME("Sixième"),
    CINQUIEME("Cinquième"),
    QUATRIEME("Quatrième"),
    TROISIEME("Troisième")
}
