package fr.learnandrun.android.viewmodel.general

import androidx.lifecycle.viewModelScope
import fr.learnandrun.android.R
import fr.learnandrun.android.business.CurrentUser
import fr.learnandrun.android.service.UserService
import fr.learnandrun.android.service.model.user.ParentModel
import fr.learnandrun.android.service.model.user.ProfessorModel
import fr.learnandrun.android.service.model.user.StudentModel
import fr.learnandrun.android.tools.MessageFormat
import fr.learnandrun.android.tools.delegate.DelegateLiveData
import fr.learnandrun.android.tools.doWhileNullWithDelayUntilRetry
import fr.learnandrun.android.tools.minimumDelay
import fr.learnandrun.android.view.ui.fragment.general.WaitingFragmentDirections
import fr.learnandrun.android.viewmodel.BasicViewModel
import fr.learnandrun.android.viewmodel.alert.InfoAlert
import kotlinx.coroutines.launch

class WaitingViewModel(
    val currentUser: CurrentUser,
    val userService: UserService
) : BasicViewModel() {

    val statusLive = DelegateLiveData<MessageFormat>()
    var status: MessageFormat by statusLive

    fun start() {
        viewModelScope.launch {
            val isAuth = minimumDelay(1500) {
                doWhileNullWithDelayUntilRetry(5000) { nbTry ->
                    userService.isAuth {
                        onError {
                            if (it.isTimeout())
                                status = MessageFormat.from(R.string.timeout_retrying, nbTry)
                            else
                                InfoAlert().apply {
                                    textId = R.string.error_occurred
                                    borderColor = R.color.colorRed
                                }.showSuspendAlert()
                        }
                    }.result
                }.also { status = MessageFormat(R.string.connected) }
            }
            if (isAuth) {
                userService.getInfo {
                    onSuccess {
                        currentUser.userModel = it
                        when (it) {
                            is StudentModel -> navigate(
                                WaitingFragmentDirections.actionWaitingFragmentToStudentHomeFragment()
                            )
                            is ParentModel -> navigate(
                                WaitingFragmentDirections.actionWaitingFragmentToParentHomeFragment()
                            )
                            is ProfessorModel -> TODO()
                            else -> throw IllegalStateException("The user model type is unknown")
                        }
                    }
                    onError {
                        val msg: String? = it.apiCallErrorResponse?.message
                        InfoAlert().apply {
                            textMessage = MessageFormat.from(msg ?: R.string.error_occurred)
                            borderColor = R.color.colorRed
                        }.showSuspendAlert()
                    }
                }
            } else
                navigate(WaitingFragmentDirections.actionWaitingFragmentToLoginFragment())
        }
    }

}