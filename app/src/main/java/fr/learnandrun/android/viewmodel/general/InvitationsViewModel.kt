package fr.learnandrun.android.viewmodel.general

import androidx.lifecycle.viewModelScope
import fr.learnandrun.android.R
import fr.learnandrun.android.service.UserService
import fr.learnandrun.android.service.model.user.InvitationModel
import fr.learnandrun.android.tools.delegate.DelegateLiveData
import fr.learnandrun.android.tools.delegate.DelegateNullableLiveData
import fr.learnandrun.android.viewmodel.BasicViewModel
import fr.learnandrun.android.viewmodel.alert.InfoAlert
import kotlinx.coroutines.launch

class InvitationsViewModel(
    val userService: UserService
) : BasicViewModel() {

    val listInvitationsLive = DelegateNullableLiveData<List<InvitationModel>>()
    var listInvitations by listInvitationsLive

    val isRefreshingLive = DelegateLiveData(false)
    var isRefreshing by isRefreshingLive

    fun back() {
        viewModelScope.launch {
            navigateBack()
        }
    }

    init {
        viewModelScope.launch {
            refreshInvitations()
        }
    }

    suspend fun refreshInvitations() {
        isRefreshing = true
        userService.getPendingRelations {
            onSuccess {
                listInvitations = it
            }
            onError {
                InfoAlert().apply {
                    textId = R.string.error_occurred
                }.showSuspendAlert()
            }
        }
        isRefreshing = false
    }

    fun declineInvitation(invitationModel: InvitationModel) {
        viewModelScope.launch {
            showLoadAlert()
            userService.declineInvitation(invitationModel.id) {
                onPreHandle {
                    hideLoadAlert()
                }
                onSuccess {
                    refreshInvitations()
                }
                onError {
                    InfoAlert().apply {
                        textId = R.string.error_occurred
                    }.showSuspendAlert()
                }
            }
        }
    }

    fun acceptInvitation(invitationModel: InvitationModel) {
        viewModelScope.launch {
            showLoadAlert()
            userService.validateRelation(invitationModel.id) {
                onPreHandle {
                    hideLoadAlert()
                }
                onSuccess {
                    refreshInvitations()
                }
                onError {
                    InfoAlert().apply {
                        textId = R.string.error_occurred
                    }.showSuspendAlert()
                }
            }
        }
    }

}