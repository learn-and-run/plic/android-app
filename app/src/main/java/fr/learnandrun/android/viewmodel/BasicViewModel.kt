package fr.learnandrun.android.viewmodel

import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController
import fr.learnandrun.android.tools.MessageFormat
import fr.learnandrun.android.tools.event.LiveEvent
import fr.learnandrun.android.tools.handler.SetupSuccessOrCancel
import fr.learnandrun.android.tools.handler.SuccessOrCancelHandler.Companion.handleFunction
import fr.learnandrun.android.tools.hideKeyboard
import fr.learnandrun.android.tools.toast
import fr.learnandrun.android.view.ui.fragment.AlertDialogFragment
import fr.learnandrun.android.viewmodel.alert.Alert
import fr.learnandrun.android.viewmodel.alert.ProgressAlert
import kotlinx.coroutines.delay

open class BasicViewModel : ViewModel() {

    var alert: Alert<*>? = null
    private val alertProgress = ProgressAlert()

    val alertEvent = LiveEvent<Alert<*>>()
    private val toastEvent = LiveEvent<MessageFormat>()
    private val navigateEvent = LiveEvent<NavDirections?>()
    private val hideKeyboardEvent = LiveEvent<Unit>()

    fun showLoadAlert() {
        hideKeyboard()
        alertProgress.showAlert()
    }

    fun hideLoadAlert() {
        alertProgress.resume()
    }

    suspend inline fun <reified T : Any> Alert<T>.showSuspendAlert(
        setupInterface: SetupSuccessOrCancel<T> = SetupSuccessOrCancel { }
    ): T? {
        this@BasicViewModel.alert = this
        alertEvent.trigger(this)
        return handleFunction(setupInterface, ::waitResponse)
    }

    fun Alert<*>.showAlert() {
        this@BasicViewModel.alert = this
        alertEvent.trigger(this)
    }

    fun hideKeyboard() {
        hideKeyboardEvent.trigger(Unit)
    }

    fun toast(format: Any, vararg args: Any) =
        toastEvent.trigger(MessageFormat(format, args.toList()))

    suspend fun navigate(navDirections: NavDirections) {
        navigateEvent.trigger(navDirections)
        delay(1)
    }

    suspend fun navigateBack() {
        navigateEvent.trigger(null)
        delay(1)
    }


    fun linkBasicEventToFragment(fragment: Fragment, view: View) {
        fragment.apply {
            toastEvent.subscribe(viewLifecycleOwner) {
                toast(it.getMessage(fragment.requireContext()))
            }
            navigateEvent.subscribe(viewLifecycleOwner) {
                if (it == null)
                    findNavController().popBackStack()
                else
                    findNavController().navigate(it)
            }
            hideKeyboardEvent.subscribe(viewLifecycleOwner) {
                requireContext().hideKeyboard(view)
            }
            alertEvent.subscribe(viewLifecycleOwner) {
                val alertDialogFragment = AlertDialogFragment()
                alertDialogFragment.setTargetFragment(this, 0)
                alertDialogFragment.show(parentFragmentManager, null)
            }
        }
    }

}