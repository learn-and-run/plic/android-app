package fr.learnandrun.android.viewmodel.alert

import android.view.View
import fr.learnandrun.android.R
import fr.learnandrun.android.view.ui.fragment.AlertDialogFragment
import kotlinx.android.synthetic.main.alert_progress.view.*
import kotlinx.coroutines.delay
import kotlin.coroutines.Continuation
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

class ProgressAlert : Alert<Unit>() {

    override val layoutId: Int = R.layout.alert_progress

    private var progressAlertContinuation: Continuation<Unit?>? = null

    init {
        isCancelable = false
        backgroundColor = R.color.colorTransparent
        borderColor = R.color.colorTransparent
    }

    override suspend fun waitResponse(): Unit? {
        val result = suspendCoroutine<Unit?> { progressAlertContinuation = it }
        delay(1)
        return result
    }

    override fun resumeWith(value: Unit?) {
        progressAlertContinuation?.resume(value)
        progressAlertContinuation = null
        dialogDismiss?.invoke()
    }

    fun resume() = resumeWith(Unit)

    override fun setupView(dialogFragment: AlertDialogFragment, view: View) {
        with(view) {
            setupBase(dialogFragment, view, alert_progress, alert_progress_inner_layout, alert_progress_text_view)
            fun updateVisibility() = with(alert_progress_text_view) {
                visibility = if (textId == R.string.no_text && textMessage == null) View.GONE else View.VISIBLE
            }
            textIdLive.observe(dialogFragment.viewLifecycleOwner) {
                updateVisibility()
            }
            textMessageLive.observe(dialogFragment.viewLifecycleOwner) {
                updateVisibility()
            }
            updateVisibility()
        }
    }

}