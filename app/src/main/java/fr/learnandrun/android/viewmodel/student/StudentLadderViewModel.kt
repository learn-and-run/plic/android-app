package fr.learnandrun.android.viewmodel.student

import androidx.lifecycle.viewModelScope
import fr.learnandrun.android.R
import fr.learnandrun.android.business.CurrentUser
import fr.learnandrun.android.service.ScoreService
import fr.learnandrun.android.service.UserService
import fr.learnandrun.android.service.model.user.LadderModel
import fr.learnandrun.android.service.model.user.ModuleType
import fr.learnandrun.android.tools.delegate.DelegateLiveData
import fr.learnandrun.android.tools.delegate.DelegateNullableLiveData
import fr.learnandrun.android.viewmodel.BasicViewModel
import fr.learnandrun.android.viewmodel.alert.InfoAlert
import kotlinx.coroutines.launch

class StudentLadderViewModel(
    val currentUser: CurrentUser,
    val userService: UserService,
    val scoreService: ScoreService
) : BasicViewModel() {

    var selectedModule = ModuleType.GENERAL
    val modules: List<ModuleType> = ModuleType.values().toList()
    val listLadderLive = DelegateNullableLiveData<List<LadderModel>>()
    var listLadder by listLadderLive

    val isRefreshingLive = DelegateLiveData(false)
    var isRefreshing by isRefreshingLive

    fun back() {
        viewModelScope.launch {
            navigateBack()
        }
    }

    init {
        viewModelScope.launch {
            refreshList()
        }
    }

    suspend fun refreshList() {
        isRefreshing = true
        scoreService.getFriendsScoreByModule(
            selectedModule
        ) {
            onSuccess {
                listLadder = it
            }
            onError {
                InfoAlert().apply {
                    textId = R.string.error_occurred
                }.showSuspendAlert()
            }
        }
        isRefreshing = false
    }

    suspend fun moduleClick(moduleType: ModuleType) {
        selectedModule = moduleType
        refreshList()
    }

}