package fr.learnandrun.android.viewmodel.alert

import android.view.View
import androidx.core.content.ContextCompat
import fr.learnandrun.android.R
import fr.learnandrun.android.tools.setPushAndOnClick
import fr.learnandrun.android.view.ui.element.ColorSet
import fr.learnandrun.android.view.ui.fragment.AlertDialogFragment
import kotlinx.android.synthetic.main.alert_ask.view.*
import kotlinx.coroutines.delay
import kotlin.coroutines.Continuation
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

class AskAlert : Alert<Boolean>() {

    override val layoutId: Int = R.layout.alert_ask

    var buttonYesColorSet: ColorSet = defaultButtonYesColorSet
    var buttonNoColorSet: ColorSet = defaultButtonNoColorSet

    private var askAlertContinuation: Continuation<Boolean?>? = null

    override suspend fun waitResponse(): Boolean? {
        val result = suspendCoroutine<Boolean?> { askAlertContinuation = it }
        delay(1)
        return result
    }

    override fun resumeWith(value: Boolean?) {
        askAlertContinuation?.resume(value)
        askAlertContinuation = null
        dialogDismiss?.invoke()
    }

    override fun setupView(dialogFragment: AlertDialogFragment, view: View) {
        with(view) {
            setupBase(dialogFragment, view, alert_ask, alert_ask_inner_layout, alert_ask_text_view)
            alert_ask_yes_button_view.activatedColors = ColorSet(
                ContextCompat.getColor(context, buttonYesColorSet.background),
                ContextCompat.getColor(context, buttonYesColorSet.foreground)
            )
            alert_ask_no_button_view.activatedColors = ColorSet(
                ContextCompat.getColor(context, buttonNoColorSet.background),
                ContextCompat.getColor(context, buttonNoColorSet.foreground)
            )
            alert_ask_yes_button_view.setActive(true)
            alert_ask_no_button_view.setActive(true)

            alert_ask_yes_button_view.setPushAndOnClick {
                resumeWith(true)
            }
            alert_ask_no_button_view.setPushAndOnClick {
                resumeWith(false)
            }
        }
    }

    companion object {
        val defaultButtonYesColorSet = ColorSet(R.color.colorYellow, R.color.colorGreen)
        val defaultButtonNoColorSet = ColorSet(R.color.colorRed, R.color.colorWhite)
    }

}