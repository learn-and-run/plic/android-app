package fr.learnandrun.android.viewmodel.alert

import android.view.View
import androidx.core.content.ContextCompat
import fr.learnandrun.android.R
import fr.learnandrun.android.tools.setPushAndOnClick
import fr.learnandrun.android.view.ui.element.ColorSet
import fr.learnandrun.android.view.ui.fragment.AlertDialogFragment
import kotlinx.android.synthetic.main.alert_info.view.*
import kotlinx.coroutines.delay
import kotlin.coroutines.Continuation
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

class InfoAlert : Alert<Unit>() {

    override val layoutId: Int = R.layout.alert_info

    var buttonOkColorSet: ColorSet = defaultButtonOkColorSet

    private var infoAlertContinuation: Continuation<Unit?>? = null

    override suspend fun waitResponse(): Unit? {
        val result = suspendCoroutine<Unit?> { infoAlertContinuation = it }
        delay(1)
        return result
    }

    override fun resumeWith(value: Unit?) {
        infoAlertContinuation?.resume(value)
        infoAlertContinuation = null
        dialogDismiss?.invoke()
    }

    fun resume() = resumeWith(Unit)

    override fun setupView(dialogFragment: AlertDialogFragment, view: View) {
        with(view) {
            setupBase(dialogFragment, view, alert_info, alert_info_inner_layout, alert_info_text_view)
            alert_info_ok_button_view.activatedColors = ColorSet(
                ContextCompat.getColor(context, buttonOkColorSet.background),
                ContextCompat.getColor(context, buttonOkColorSet.foreground)
            )
            alert_info_ok_button_view.setActive(true)

            alert_info_ok_button_view.setPushAndOnClick {
                resume()
            }
        }
    }

    companion object {
        val defaultButtonOkColorSet = ColorSet(R.color.colorYellow, R.color.colorGreen)
    }

}