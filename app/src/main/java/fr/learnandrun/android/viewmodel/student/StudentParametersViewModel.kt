package fr.learnandrun.android.viewmodel.student

import android.text.InputType
import androidx.lifecycle.viewModelScope
import fr.learnandrun.android.R
import fr.learnandrun.android.business.CurrentUser
import fr.learnandrun.android.service.UserService
import fr.learnandrun.android.service.model.user.StudentModel
import fr.learnandrun.android.tools.MessageFormat
import fr.learnandrun.android.tools.delegate.DelegateLiveData
import fr.learnandrun.android.tools.delegate.DelegateLiveText
import fr.learnandrun.android.view.ui.element.ColorSet
import fr.learnandrun.android.view.ui.fragment.student.StudentParametersFragmentDirections
import fr.learnandrun.android.viewmodel.BasicViewModel
import fr.learnandrun.android.viewmodel.alert.AskAlert
import fr.learnandrun.android.viewmodel.alert.InfoAlert
import fr.learnandrun.android.viewmodel.alert.InputAlert
import kotlinx.coroutines.launch

class StudentParametersViewModel(
    val currentUser: CurrentUser,
    val userService: UserService
) : BasicViewModel() {

    val studentModel = currentUser.userModel as? StudentModel
        ?: throw IllegalStateException("The user must be a student model")

    val lastNameFieldLive = DelegateLiveText(studentModel.lastname) {
        validateIsActive = anyFieldHaveChanged()
    }
    val firstNameFieldLive = DelegateLiveText(studentModel.firstname) {
        validateIsActive = anyFieldHaveChanged()
    }
    val pseudoFieldLive = DelegateLiveText(studentModel.pseudo) {
        validateIsActive = anyFieldHaveChanged()
    }
    val classeLive = DelegateLiveData(studentModel.levelType)

    var lastNameField by lastNameFieldLive
    var firstNameField by firstNameFieldLive
    var pseudoField by pseudoFieldLive
    var classe
        get() = classeLive.value
        set(value) {
            classeLive.value = value
            validateIsActive = anyFieldHaveChanged()
        }

    val validateIsActiveLive = DelegateLiveData(false)
    var validateIsActive by validateIsActiveLive

    private fun anyFieldHaveChanged(): Boolean = lastNameField != studentModel.lastname ||
            firstNameField != studentModel.firstname ||
            pseudoField != studentModel.pseudo ||
            classe != studentModel.levelType

    fun logout() {
        hideKeyboard()
        viewModelScope.launch {
            AskAlert().apply {
                textId = R.string.alert_logout
            }.showSuspendAlert {
                onSuccess { userWantToDisconnect ->
                    if (!userWantToDisconnect) return@onSuccess
                    showLoadAlert()
                    userService.logout {
                        onPreHandle {
                            hideLoadAlert()
                        }
                    }
                    navigate(
                        StudentParametersFragmentDirections.actionStudentParametersFragmentToLoginFragment()
                    )
                }
            }
        }
    }

    private suspend fun confirmChangePassword(): Boolean {
        return AskAlert().apply {
            textId = R.string.ask_update_password
        }.showSuspendAlert() == true
    }

    private suspend fun fullyAuthUser(): Boolean {
        var isFullyAuth = false
        InputAlert().apply {
            this.hintTextId = R.string.password
            this.textId = R.string.confirm_password
            this.textType = InputType.TYPE_TEXT_VARIATION_PASSWORD
        }.showSuspendAlert {
            onSuccess { password ->
                showLoadAlert()
                userService.login(studentModel.pseudo, password, true) {
                    minimumDelay = 500
                    onPreHandle {
                        hideLoadAlert()
                    }
                    onSuccess {
                        isFullyAuth = true
                    }
                    onError {
                        InfoAlert().apply {
                            textMessage = MessageFormat.from(it.message ?: R.string.error_occurred)
                        }.showSuspendAlert()
                    }
                }
            }
        }
        return isFullyAuth
    }

    private suspend fun askNewInputText(textId: Int, isPassword: Boolean = true): String? {
        return InputAlert().apply {
            this.hintTextId = if (isPassword) R.string.password else R.string.email
            this.textId = textId
            if (isPassword)
                this.textType = InputType.TYPE_TEXT_VARIATION_PASSWORD
        }.showSuspendAlert()
    }

    fun changePassword() {
        hideKeyboard()
        viewModelScope.launch {
            if (confirmChangePassword() && fullyAuthUser()) {
                var firstNewPassword: String
                var secondNewPassword: String

                firstNewPassword = askNewInputText(R.string.enter_new_password) ?: return@launch
                secondNewPassword = askNewInputText(R.string.confirm_new_password) ?: return@launch
                while (firstNewPassword != secondNewPassword) {
                    InfoAlert().apply { textId = R.string.wrong_second_password }.showSuspendAlert()
                    firstNewPassword = askNewInputText(R.string.enter_new_password) ?: return@launch
                    secondNewPassword = askNewInputText(R.string.confirm_new_password) ?: return@launch
                }
                showLoadAlert()
                userService.updatePassword(firstNewPassword) {
                    minimumDelay = 500
                    onPreHandle {
                        hideLoadAlert()
                    }
                    onSuccess {
                        InfoAlert().apply { textId = R.string.password_successfully_modified }.showSuspendAlert()
                    }
                    onError {
                        InfoAlert().apply {
                            textMessage = MessageFormat.from(it.message ?: R.string.error_occurred)
                        }.showSuspendAlert()
                    }
                }
            }

        }
    }

    private suspend fun confirmChangeEmail(): Boolean {
        return AskAlert().apply {
            textId = R.string.confirm_change_email
        }.showSuspendAlert() == true
    }

    fun changeEmail() {
        hideKeyboard()
        viewModelScope.launch {
            if (confirmChangeEmail() && fullyAuthUser()) {
                val newEmail = askNewInputText(R.string.enter_new_email, false) ?: return@launch

                showLoadAlert()
                userService.updateEmail(newEmail) {
                    minimumDelay = 500
                    onPreHandle {
                        hideLoadAlert()
                    }
                    onSuccess {
                        InfoAlert().apply { textId = R.string.email_successfully_modified }.showSuspendAlert()
                    }
                    onError {
                        InfoAlert().apply {
                            textMessage = MessageFormat.from(it.message ?: R.string.error_occurred)
                        }.showSuspendAlert()
                    }
                }
            }

        }
    }

    fun back() {
        viewModelScope.launch {
            navigateBack()
        }
    }

    fun validate() {
        hideKeyboard()
        viewModelScope.launch {
            AskAlert().apply {
                textId = R.string.alert_update_profile
            }.showSuspendAlert {
                onSuccess { isUserSure ->
                    if (!isUserSure) return@onSuccess
                    showLoadAlert()
                    userService.updateStudent(pseudoField, firstNameField, lastNameField, classe) {
                        onPreHandle {
                            hideLoadAlert()
                        }
                        onSuccess {
                            studentModel.firstname = it.firstname
                            studentModel.lastname = it.lastname
                            studentModel.pseudo = it.pseudo
                            studentModel.levelType = it.levelType
                            validateIsActive = anyFieldHaveChanged()
                            InfoAlert().apply {
                                textId = R.string.profile_modified
                            }.showSuspendAlert()
                        }
                        onError {
                            InfoAlert().apply {
                                textId = R.string.error_occurred
                            }.showSuspendAlert()
                        }
                    }
                }
            }
        }
    }

    fun deleteAccount() {
        hideKeyboard()
        viewModelScope.launch {
            AskAlert().apply {
                textId = R.string.alert_delete_account
                borderColor = R.color.colorRed
                buttonYesColorSet = ColorSet(R.color.colorRed, R.color.colorWhite)
                buttonNoColorSet = ColorSet(R.color.colorYellow, R.color.colorGreen)
            }.showSuspendAlert {
                onSuccess { userIsAskingToDeleteAccount ->
                    if (!userIsAskingToDeleteAccount) return@onSuccess
                    showLoadAlert()
                    userService.delete {
                        onPreHandle {
                            hideLoadAlert()
                        }
                        onSuccess {
                            InfoAlert().apply {
                                textId = R.string.account_deleted
                            }.showSuspendAlert()
                            navigate(
                                StudentParametersFragmentDirections.actionStudentParametersFragmentToLoginFragment()
                            )
                        }
                        onError {
                            InfoAlert().apply {
                                textId = R.string.error_occurred
                            }.showSuspendAlert()
                        }
                    }
                }
            }
        }
    }

}