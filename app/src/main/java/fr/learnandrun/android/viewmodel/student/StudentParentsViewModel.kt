package fr.learnandrun.android.viewmodel.student

import androidx.lifecycle.viewModelScope
import fr.learnandrun.android.R
import fr.learnandrun.android.service.UserService
import fr.learnandrun.android.service.model.user.RelationModel
import fr.learnandrun.android.service.model.user.UserType
import fr.learnandrun.android.tools.MessageFormat
import fr.learnandrun.android.tools.delegate.DelegateLiveData
import fr.learnandrun.android.tools.delegate.DelegateNullableLiveData
import fr.learnandrun.android.viewmodel.BasicViewModel
import fr.learnandrun.android.viewmodel.alert.InfoAlert
import fr.learnandrun.android.viewmodel.alert.InputAlert
import kotlinx.coroutines.launch

class StudentParentsViewModel(
    val userService: UserService
) : BasicViewModel() {

    val refreshingLive = DelegateLiveData(false)
    var refreshing by refreshingLive

    val listParentRelationsLive = DelegateNullableLiveData<List<RelationModel>>()
    var listParentRelations by listParentRelationsLive

    fun back() {
        viewModelScope.launch {
            navigateBack()
        }
    }

    init {
        viewModelScope.launch {
            refreshParents()
        }
    }

    suspend fun refreshParents() {
        refreshing = true
        userService.getAllRelations(UserType.PARENT) {
            minimumDelay = 500
            onSuccess {
                listParentRelations = it
            }
            onError {
                InfoAlert().apply {
                    textId = R.string.error_occurred
                }.showSuspendAlert()
            }
        }
        refreshing = false
    }

    fun parentTrash(parentRelation: RelationModel) {
        viewModelScope.launch {
            refreshing = true
            userService.deleteRelation(parentRelation.id) {
                onSuccess {
                    refreshParents()
                }
                onError {
                    InfoAlert().apply {
                        textMessage = MessageFormat(it.message ?: R.string.error_occurred)
                    }.showSuspendAlert()
                }
            }
            refreshing = false
        }
    }

    fun addParent() {
        viewModelScope.launch {
            val username: String = InputAlert().apply {
                hintTextId = R.string.id
                textId = R.string.input_add_parent
            }.showSuspendAlert() ?: return@launch
            showLoadAlert()
            userService.addRelation(username) {
                onPreHandle {
                    hideLoadAlert()
                }
                onSuccess {
                    InfoAlert().apply {
                        textId = R.string.invitation_sent
                    }.showSuspendAlert()
                }
                onError {
                    InfoAlert().apply {
                        textMessage = MessageFormat(it.message ?: R.string.error_occurred)
                    }.showSuspendAlert()
                }
            }
        }
    }

}