package fr.learnandrun.android.viewmodel.general

import androidx.lifecycle.viewModelScope
import fr.learnandrun.android.R
import fr.learnandrun.android.service.UserService
import fr.learnandrun.android.service.model.user.LevelType
import fr.learnandrun.android.tools.MessageFormat
import fr.learnandrun.android.tools.delegate.DelegateLiveData
import fr.learnandrun.android.tools.delegate.DelegateLiveText
import fr.learnandrun.android.tools.delegate.DelegateNullableLiveData
import fr.learnandrun.android.view.ui.fragment.general.CreateAccountStudentFragmentDirections
import fr.learnandrun.android.viewmodel.BasicViewModel
import fr.learnandrun.android.viewmodel.alert.InfoAlert
import kotlinx.coroutines.launch

class CreateAccountStudentViewModel(private val userService: UserService) : BasicViewModel() {

    val emailLiveText: DelegateLiveText = DelegateLiveText {
        createIsActive = allFieldsAreFilled()
    }
    val lastnameLiveText: DelegateLiveText = DelegateLiveText {
        createIsActive = allFieldsAreFilled()
    }
    val firstnameLiveText: DelegateLiveText = DelegateLiveText {
        createIsActive = allFieldsAreFilled()
    }
    val pseudoLiveText: DelegateLiveText = DelegateLiveText {
        createIsActive = allFieldsAreFilled()
    }
    val classeLive = DelegateNullableLiveData<LevelType>(null)
    val passwordLiveText: DelegateLiveText = DelegateLiveText {
        createIsActive = allFieldsAreFilled()
    }

    val createIsActiveLive = DelegateLiveData(false)
    var createIsActive by createIsActiveLive

    var email by emailLiveText
    var lastname by lastnameLiveText
    var firstname by firstnameLiveText
    var pseudo by pseudoLiveText
    var classe by classeLive
    var password by passwordLiveText

    private fun allFieldsAreFilled(): Boolean = classe != null &&
            listOf(email, lastname, firstname, pseudo, password).all { it.isNotBlank() }

    fun create() {
        if (!allFieldsAreFilled()) return
        val classe = classe ?: return
        showLoadAlert()
        viewModelScope.launch {
            userService.createStudent(pseudo, firstname, lastname, password, email, classe) {
                minimumDelay = 1000
                onPreHandle {
                    hideLoadAlert()
                }
                onSuccess {
                    InfoAlert().apply {
                        textId = R.string.account_created
                    }.showSuspendAlert()
                    navigate(
                        CreateAccountStudentFragmentDirections
                            .actionCreateAccountStudentFragmentToLoginFragment()
                    )
                }
                onError {
                    InfoAlert().apply {
                        textMessage = MessageFormat.from(it.message ?: R.string.error_occurred)
                    }.showSuspendAlert()
                }
            }
        }

    }

    fun back() = viewModelScope.launch {
        navigateBack()
    }

}
