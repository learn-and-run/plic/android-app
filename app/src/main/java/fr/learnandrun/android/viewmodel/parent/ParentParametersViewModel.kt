package fr.learnandrun.android.viewmodel.parent

import android.text.InputType
import androidx.lifecycle.viewModelScope
import fr.learnandrun.android.R
import fr.learnandrun.android.business.CurrentUser
import fr.learnandrun.android.service.UserService
import fr.learnandrun.android.service.model.user.ParentModel
import fr.learnandrun.android.tools.MessageFormat
import fr.learnandrun.android.tools.delegate.DelegateLiveData
import fr.learnandrun.android.tools.delegate.DelegateLiveText
import fr.learnandrun.android.view.ui.element.ColorSet
import fr.learnandrun.android.view.ui.fragment.parent.ParentParametersFragmentDirections
import fr.learnandrun.android.viewmodel.BasicViewModel
import fr.learnandrun.android.viewmodel.alert.AskAlert
import fr.learnandrun.android.viewmodel.alert.InfoAlert
import fr.learnandrun.android.viewmodel.alert.InputAlert
import kotlinx.coroutines.launch

class ParentParametersViewModel(
    currentUser: CurrentUser,
    val userService: UserService
) : BasicViewModel() {

    val parentModel = currentUser.userModel as? ParentModel
        ?: throw IllegalStateException("The user must be a parent model")

    val lastNameFieldLive = DelegateLiveText(parentModel.lastname) {
        validateIsActive = anyFieldHaveChanged()
    }
    val firstNameFieldLive = DelegateLiveText(parentModel.firstname) {
        validateIsActive = anyFieldHaveChanged()
    }
    val pseudoFieldLive = DelegateLiveText(parentModel.pseudo) {
        validateIsActive = anyFieldHaveChanged()
    }

    var lastNameField by lastNameFieldLive
    var firstNameField by firstNameFieldLive
    var pseudoField by pseudoFieldLive

    val validateIsActiveLive = DelegateLiveData(false)
    var validateIsActive by validateIsActiveLive

    private fun anyFieldHaveChanged(): Boolean = lastNameField != parentModel.lastname ||
            firstNameField != parentModel.firstname ||
            pseudoField != parentModel.pseudo

    fun logout() {
        hideKeyboard()
        viewModelScope.launch {
            AskAlert().apply {
                textId = R.string.alert_logout
            }.showSuspendAlert {
                onSuccess { userWantToDisconnect ->
                    if (!userWantToDisconnect) return@onSuccess
                    showLoadAlert()
                    userService.logout {
                        onPreHandle {
                            hideLoadAlert()
                        }
                    }
                    navigate(
                        ParentParametersFragmentDirections.actionParentParametersFragmentToLoginFragment()
                    )
                }
            }
        }
    }

    private suspend fun confirmChangePassword(): Boolean {
        return AskAlert().apply {
            textId = R.string.ask_update_password
        }.showSuspendAlert() == true
    }

    private suspend fun fullyAuthUser(): Boolean {
        var isFullyAuth = false
        InputAlert().apply {
            this.hintTextId = R.string.password
            this.textId = R.string.confirm_password
            this.textType = InputType.TYPE_TEXT_VARIATION_PASSWORD
        }.showSuspendAlert {
            onSuccess { password ->
                showLoadAlert()
                userService.login(parentModel.pseudo, password, true) {
                    minimumDelay = 500
                    onPreHandle {
                        hideLoadAlert()
                    }
                    onSuccess {
                        isFullyAuth = true
                    }
                    onError {
                        InfoAlert().apply {
                            textMessage = MessageFormat.from(it.message ?: R.string.error_occurred)
                        }.showSuspendAlert()
                    }
                }
            }
        }
        return isFullyAuth
    }

    private suspend fun askNewInputText(textId: Int, isPassword: Boolean = true): String? {
        return InputAlert().apply {
            this.hintTextId = if (isPassword) R.string.password else R.string.email
            this.textId = textId
            if (isPassword)
                this.textType = InputType.TYPE_TEXT_VARIATION_PASSWORD
        }.showSuspendAlert()
    }

    fun changePassword() {
        hideKeyboard()
        viewModelScope.launch {
            if (confirmChangePassword() && fullyAuthUser()) {
                var firstNewPassword: String
                var secondNewPassword: String

                firstNewPassword = askNewInputText(R.string.enter_new_password) ?: return@launch
                secondNewPassword = askNewInputText(R.string.confirm_new_password) ?: return@launch
                while (firstNewPassword != secondNewPassword) {
                    InfoAlert().apply { textId = R.string.wrong_second_password }.showSuspendAlert()
                    firstNewPassword = askNewInputText(R.string.enter_new_password) ?: return@launch
                    secondNewPassword = askNewInputText(R.string.confirm_new_password) ?: return@launch
                }
                showLoadAlert()
                userService.updatePassword(firstNewPassword) {
                    minimumDelay = 500
                    onPreHandle {
                        hideLoadAlert()
                    }
                    onSuccess {
                        InfoAlert().apply { textId = R.string.password_successfully_modified }.showSuspendAlert()
                    }
                    onError {
                        InfoAlert().apply {
                            textMessage = MessageFormat.from(it.message ?: R.string.error_occurred)
                        }.showSuspendAlert()
                    }
                }
            }
        }
    }

    private suspend fun confirmChangeEmail(): Boolean {
        return AskAlert().apply {
            textId = R.string.confirm_change_email
        }.showSuspendAlert() == true
    }

    fun changeEmail() {
        hideKeyboard()
        viewModelScope.launch {
            if (confirmChangeEmail() && fullyAuthUser()) {
                val newEmail = askNewInputText(R.string.enter_new_email, false) ?: return@launch

                showLoadAlert()
                userService.updateEmail(newEmail) {
                    minimumDelay = 500
                    onPreHandle {
                        hideLoadAlert()
                    }
                    onSuccess {
                        InfoAlert().apply { textId = R.string.email_successfully_modified }.showSuspendAlert()
                    }
                    onError {
                        InfoAlert().apply {
                            textMessage = MessageFormat.from(it.message ?: R.string.error_occurred)
                        }.showSuspendAlert()
                    }
                }
            }
        }
    }

    fun back() {
        viewModelScope.launch {
            navigateBack()
        }
    }

    fun validate() {
        hideKeyboard()
        viewModelScope.launch {
            AskAlert().apply {
                textId = R.string.alert_update_profile
            }.showSuspendAlert {
                onSuccess { isUserSure ->
                    if (!isUserSure) return@onSuccess
                    showLoadAlert()
                    userService.updateParent(pseudoField, firstNameField, lastNameField) {
                        onPreHandle {
                            hideLoadAlert()
                        }
                        onSuccess {
                            parentModel.firstname = it.firstname
                            parentModel.lastname = it.lastname
                            parentModel.pseudo = it.pseudo
                            validateIsActive = anyFieldHaveChanged()
                            InfoAlert().apply {
                                textId = R.string.profile_modified
                            }.showSuspendAlert()
                        }
                        onError {
                            InfoAlert().apply {
                                textId = R.string.error_occurred
                            }.showSuspendAlert()
                        }
                    }
                }
            }
        }
    }

    fun deleteAccount() {
        hideKeyboard()
        viewModelScope.launch {
            AskAlert().apply {
                textId = R.string.alert_delete_account
                borderColor = R.color.colorRed
                buttonYesColorSet = ColorSet(R.color.colorRed, R.color.colorWhite)
                buttonNoColorSet = ColorSet(R.color.colorYellow, R.color.colorGreen)
            }.showSuspendAlert {
                onSuccess { userIsAskingToDeleteAccount ->
                    if (!userIsAskingToDeleteAccount) return@onSuccess
                    showLoadAlert()
                    userService.delete {
                        onPreHandle {
                            hideLoadAlert()
                        }
                        onSuccess {
                            InfoAlert().apply {
                                textId = R.string.account_deleted
                            }.showSuspendAlert()
                            navigate(
                                ParentParametersFragmentDirections.actionParentParametersFragmentToLoginFragment()
                            )
                        }
                        onError {
                            InfoAlert().apply {
                                textId = R.string.error_occurred
                            }.showSuspendAlert()
                        }
                    }
                }
            }
        }
    }
}