package fr.learnandrun.android.viewmodel.general

import androidx.lifecycle.viewModelScope
import fr.learnandrun.android.R
import fr.learnandrun.android.business.CurrentUser
import fr.learnandrun.android.service.ScoreService
import fr.learnandrun.android.service.UserService
import fr.learnandrun.android.service.model.score.ScoreModel
import fr.learnandrun.android.service.model.user.ModuleType
import fr.learnandrun.android.tools.delegate.DelegateLiveData
import fr.learnandrun.android.tools.delegate.DelegateNullableLiveData
import fr.learnandrun.android.viewmodel.BasicViewModel
import fr.learnandrun.android.viewmodel.alert.InfoAlert
import kotlinx.coroutines.launch

class StatsStudentViewModel(
    val currentUser: CurrentUser,
    val userService: UserService,
    val scoreService: ScoreService
) : BasicViewModel() {

    val pseudoLive = DelegateNullableLiveData<String>()
    var pseudo: String?
        get() = pseudoLive.value
        set(value) {
            pseudoLive.value = value
            viewModelScope.launch {
                refreshList()
            }
        }

    var selectedModule = ModuleType.GENERAL
    val modules: List<ModuleType> = ModuleType.values().toList()
    val listScoresLive = DelegateNullableLiveData<List<ScoreModel>>()
    var listScores by listScoresLive

    val isRefreshingLive = DelegateLiveData(false)
    var isRefreshing by isRefreshingLive

    fun back() {
        viewModelScope.launch {
            navigateBack()
        }
    }

    init {
        viewModelScope.launch {
            refreshList()
        }
    }

    suspend fun refreshList() {
        val pseudo = pseudo ?: return
        isRefreshing = true
        scoreService.getScoresOfPseudoByModule(
            pseudo,
            selectedModule
        ) {
            onSuccess {
                listScores = it.scoreModels
            }
            onError {
                InfoAlert().apply {
                    textId = R.string.error_occurred
                }.showSuspendAlert()
            }
        }
        isRefreshing = false
    }

    suspend fun moduleClick(moduleType: ModuleType) {
        selectedModule = moduleType
        refreshList()
    }

}