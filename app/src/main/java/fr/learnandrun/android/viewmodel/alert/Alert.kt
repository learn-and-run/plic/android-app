package fr.learnandrun.android.viewmodel.alert

import android.content.res.ColorStateList
import android.view.View
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import fr.learnandrun.android.R
import fr.learnandrun.android.tools.MessageFormat
import fr.learnandrun.android.tools.delegate.DelegateLiveData
import fr.learnandrun.android.tools.delegate.DelegateNullableLiveData
import fr.learnandrun.android.view.ui.fragment.AlertDialogFragment

abstract class Alert<T : Any> {

    abstract val layoutId: Int
    var dialogDismiss: (() -> Unit)? = null
    var isCancelable = true

    var textColor: Int = defaultTextColor
    var borderColor: Int = defaultBorderColor
    var backgroundColor: Int = defaultBackgroundColor

    val textIdLive = DelegateLiveData(defaultTextId)
    var textId by textIdLive
    val textMessageLive = DelegateNullableLiveData<MessageFormat>(null)
    var textMessage by textMessageLive

    abstract suspend fun waitResponse(): T?
    abstract fun resumeWith(value: T?)
    fun dismiss() {
        dialogDismiss = null
        resumeWith(null)
    }

    abstract fun setupView(dialogFragment: AlertDialogFragment, view: View)

    fun setupBase(
        dialogFragment: AlertDialogFragment,
        view: View,
        alert_back: ConstraintLayout,
        alert_back_inner_layout: ConstraintLayout,
        alert_text_view: TextView
    ) {
        dialogFragment.isCancelable = isCancelable
        with(view) {
            alert_back.backgroundTintList = ColorStateList.valueOf(
                ContextCompat.getColor(context, borderColor)
            )
            alert_back_inner_layout.backgroundTintList = ColorStateList.valueOf(
                ContextCompat.getColor(context, backgroundColor)
            )
            textIdLive.observe(dialogFragment.viewLifecycleOwner) {
                if (textMessage == null)
                    alert_text_view.text = context.getString(textId)
            }
            textMessageLive.observe(dialogFragment.viewLifecycleOwner) {
                textMessage?.let {
                    alert_text_view.text = it.getMessage(context)
                }
            }
            alert_text_view.setTextColor(
                ContextCompat.getColor(context, textColor)
            )
        }
    }

    companion object {
        const val defaultTextId: Int = R.string.no_text
        const val defaultTextColor: Int = R.color.colorWhite
        const val defaultBorderColor: Int = R.color.colorGray
        const val defaultBackgroundColor: Int = R.color.colorGreen
    }

}