package fr.learnandrun.android.viewmodel.parent

import androidx.lifecycle.viewModelScope
import fr.learnandrun.android.R
import fr.learnandrun.android.business.CurrentUser
import fr.learnandrun.android.service.UserService
import fr.learnandrun.android.service.model.user.ParentModel
import fr.learnandrun.android.service.model.user.RelationModel
import fr.learnandrun.android.service.model.user.StudentModel
import fr.learnandrun.android.service.model.user.UserType
import fr.learnandrun.android.tools.MessageFormat
import fr.learnandrun.android.tools.delegate.DelegateLiveData
import fr.learnandrun.android.tools.delegate.DelegateNullableLiveData
import fr.learnandrun.android.view.ui.fragment.parent.ParentHomeFragmentDirections
import fr.learnandrun.android.viewmodel.BasicViewModel
import fr.learnandrun.android.viewmodel.alert.InfoAlert
import fr.learnandrun.android.viewmodel.alert.InputAlert
import kotlinx.coroutines.launch

class ParentHomeViewModel(
    val currentUser: CurrentUser,
    val userService: UserService
) : BasicViewModel() {

    val parentModel: ParentModel
        get() = currentUser.userModel as? ParentModel ?: throw IllegalStateException("Current user must be a parent")


    fun invitations() {
        viewModelScope.launch {
            navigate(ParentHomeFragmentDirections.actionParentHomeFragmentToInvitationsFragment())
        }
    }

    fun settings() {
        viewModelScope.launch {
            navigate(ParentHomeFragmentDirections.actionParentHomeFragmentToParentParametersFragment())
        }
    }

    fun addStudent() {
        viewModelScope.launch {
            val username: String = InputAlert().apply {
                hintTextId = R.string.id
                textId = R.string.input_add_child
            }.showSuspendAlert() ?: return@launch
            showLoadAlert()
            userService.addRelation(username) {
                onPreHandle {
                    hideLoadAlert()
                }
                onSuccess {
                    InfoAlert().apply {
                        textId = R.string.invitation_sent
                    }.showSuspendAlert()
                }
                onError {
                    InfoAlert().apply {
                        textMessage = MessageFormat(it.message ?: R.string.error_occurred)
                    }.showSuspendAlert()
                }
            }
        }
    }

    val isRefreshingLive = DelegateLiveData(false)
    var isRefreshing by isRefreshingLive

    val listStudentRelationsLive = DelegateNullableLiveData<List<RelationModel>>()
    var listStudentRelations by listStudentRelationsLive

    init {
        viewModelScope.launch {
            refreshList()
        }
    }

    suspend fun refreshList() {
        isRefreshing = true
        userService.getAllRelations(UserType.STUDENT) {
            onSuccess {
                listStudentRelations = it.filter { relationModel -> relationModel.userModel is StudentModel }
            }
            onError {
                InfoAlert().apply {
                    textMessage = MessageFormat(it.message ?: R.string.error_occurred)
                }.showSuspendAlert()
            }
        }
        isRefreshing = false
    }

    fun studentClick(studentModel: StudentModel) {
        viewModelScope.launch {
            navigate(ParentHomeFragmentDirections.actionParentHomeFragmentToStatsStudentFragment(studentModel.pseudo))
        }
    }

    fun studentTrash(studentRelation: RelationModel) {
        isRefreshing = true
        viewModelScope.launch {
            userService.deleteRelation(studentRelation.id) {
                onSuccess {
                    refreshList()
                }
                onError {
                    InfoAlert().apply {
                        textMessage = MessageFormat(it.message ?: R.string.error_occurred)
                    }.showSuspendAlert()
                }
            }
        }
        isRefreshing = false
    }

}