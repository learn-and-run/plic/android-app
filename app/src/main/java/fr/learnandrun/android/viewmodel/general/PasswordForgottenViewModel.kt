package fr.learnandrun.android.viewmodel.general

import androidx.lifecycle.viewModelScope
import fr.learnandrun.android.R
import fr.learnandrun.android.service.UserService
import fr.learnandrun.android.tools.MessageFormat
import fr.learnandrun.android.tools.delegate.DelegateLiveData
import fr.learnandrun.android.tools.delegate.DelegateLiveText
import fr.learnandrun.android.viewmodel.BasicViewModel
import fr.learnandrun.android.viewmodel.alert.InfoAlert
import kotlinx.coroutines.launch

class PasswordForgottenViewModel(val userService: UserService) : BasicViewModel() {

    val emailLiveText: DelegateLiveText = DelegateLiveText {
        // Triggered when the value is changed from the view
        sendButtonIsActive = email.isNotBlank()
    }

    val sendButtonIsActiveLive = DelegateLiveData(false)

    val email by emailLiveText
    var sendButtonIsActive by sendButtonIsActiveLive


    fun send() {
        if (email.isBlank()) return
        showLoadAlert()
        viewModelScope.launch {
            userService.askResetPassword(email) {
                minimumDelay = 1000
                onPreHandle {
                    hideLoadAlert()
                }
                onSuccess {
                    InfoAlert().apply {
                        textId = R.string.email_sent
                    }.showSuspendAlert()
                    navigateBack()
                }
                onError {
                    val msg = it.apiCallErrorResponse?.message
                    InfoAlert().apply {
                        textMessage = MessageFormat.from(msg ?: R.string.error_occurred)
                        borderColor = R.color.colorRed
                    }.showSuspendAlert()
                }
            }
        }
    }

    fun cancel() = viewModelScope.launch {
        navigateBack()
    }

}