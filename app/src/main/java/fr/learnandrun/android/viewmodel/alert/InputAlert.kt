package fr.learnandrun.android.viewmodel.alert

import android.text.InputType
import android.view.View
import androidx.core.content.ContextCompat
import fr.learnandrun.android.R
import fr.learnandrun.android.tools.delegate.DelegateLiveData
import fr.learnandrun.android.tools.delegate.DelegateLiveText
import fr.learnandrun.android.tools.setPushAndOnClick
import fr.learnandrun.android.view.ui.element.ColorSet
import fr.learnandrun.android.view.ui.fragment.AlertDialogFragment
import kotlinx.android.synthetic.main.alert_input.view.*
import kotlinx.coroutines.delay
import kotlin.coroutines.Continuation
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

class InputAlert : Alert<String>() {

    override val layoutId: Int = R.layout.alert_input

    var inputFieldColorSet: ColorSet = defaultInputFieldColorSet
    var textType: Int = defaultTextType
    var hintTextId: Int = R.string.no_text
    var buttonValidateColorSet: ColorSet = defaultButtonValidateColorSet
    var buttonCancelColorSet: ColorSet = defaultButtonCancelColorSet

    val validateIsActiveLive = DelegateLiveData(false)
    val textFieldLive: DelegateLiveText = DelegateLiveText {
        validateIsActive = it.isNotBlank()
    }

    var validateIsActive by validateIsActiveLive
    var textField by textFieldLive

    private var inputAlertContinuation: Continuation<String?>? = null

    override suspend fun waitResponse(): String? {
        val result = suspendCoroutine<String?> { inputAlertContinuation = it }
        delay(1)
        return result
    }

    override fun resumeWith(value: String?) {
        inputAlertContinuation?.resume(value)
        inputAlertContinuation = null
        dialogDismiss?.invoke()
    }

    override fun setupView(dialogFragment: AlertDialogFragment, view: View) {
        with(view) {
            setupBase(dialogFragment, view, alert_input, alert_input_inner_layout, alert_input_text_view)
            textFieldLive.bindEditTextView(
                dialogFragment.viewLifecycleOwner,
                alert_input_field_view.editText
            )
            alert_input_field_view.setTextType(textType)
            alert_input_field_view.activatedColors = ColorSet(
                ContextCompat.getColor(context, inputFieldColorSet.background),
                ContextCompat.getColor(context, inputFieldColorSet.foreground)
            )
            alert_input_validate_button_view.activatedColors = ColorSet(
                ContextCompat.getColor(context, buttonValidateColorSet.background),
                ContextCompat.getColor(context, buttonValidateColorSet.foreground)
            )
            alert_input_cancel_button_view.activatedColors = ColorSet(
                ContextCompat.getColor(context, buttonCancelColorSet.background),
                ContextCompat.getColor(context, buttonCancelColorSet.foreground)
            )
            alert_input_field_view.editText.hint = context.getString(hintTextId)
            alert_input_field_view.setActive(true)
            alert_input_validate_button_view.setActive(validateIsActive)
            alert_input_cancel_button_view.setActive(true)

            validateIsActiveLive.observe(dialogFragment.viewLifecycleOwner) {
                alert_input_validate_button_view.setActive(it)
            }

            alert_input_validate_button_view.setPushAndOnClick {
                resumeWith(textField)
            }
            alert_input_cancel_button_view.setPushAndOnClick {
                resumeWith(null)
            }
        }
    }

    companion object {
        val defaultInputFieldColorSet = ColorSet(R.color.colorWhite, R.color.colorWhite)
        const val defaultTextType: Int = InputType.TYPE_TEXT_VARIATION_NORMAL
        val defaultButtonValidateColorSet = ColorSet(R.color.colorYellow, R.color.colorGreen)
        val defaultButtonCancelColorSet = ColorSet(R.color.colorRed, R.color.colorWhite)
    }

}