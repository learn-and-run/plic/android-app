package fr.learnandrun.android.viewmodel.general

import androidx.lifecycle.viewModelScope
import fr.learnandrun.android.R
import fr.learnandrun.android.business.CurrentUser
import fr.learnandrun.android.service.UserService
import fr.learnandrun.android.service.model.user.ParentModel
import fr.learnandrun.android.service.model.user.ProfessorModel
import fr.learnandrun.android.service.model.user.StudentModel
import fr.learnandrun.android.tools.MessageFormat
import fr.learnandrun.android.tools.delegate.DelegateLiveData
import fr.learnandrun.android.tools.delegate.DelegateLiveText
import fr.learnandrun.android.view.ui.fragment.general.LoginFragmentDirections
import fr.learnandrun.android.viewmodel.BasicViewModel
import fr.learnandrun.android.viewmodel.alert.InfoAlert
import kotlinx.coroutines.launch

class LoginViewModel(
    val currentUser: CurrentUser,
    val userService: UserService
) : BasicViewModel() {

    val idLiveText: DelegateLiveText = DelegateLiveText {
        // Triggered when the value is changed from the view
        connectButtonIsActive = id.isNotBlank() && password.isNotBlank()
    }
    val passwordLiveText: DelegateLiveText = DelegateLiveText {
        // Triggered when the value is changed from the view
        connectButtonIsActive = id.isNotBlank() && password.isNotBlank()
    }

    val connectButtonIsActiveLive = DelegateLiveData(false)

    val id by idLiveText
    val password by passwordLiveText

    var connectButtonIsActive by connectButtonIsActiveLive

    fun connect() {
        if (id.isBlank() || password.isBlank()) return
        viewModelScope.launch {
            showLoadAlert()
            userService.login(id, password, true) {
                minimumDelay = 1000
                onSuccess {
                    userService.getInfo {
                        onPreHandle {
                            hideLoadAlert()
                        }
                        onSuccess {
                            currentUser.userModel = it
                            when (it) {
                                is StudentModel -> navigate(LoginFragmentDirections.actionLoginFragmentToStudentHomeFragment())
                                is ParentModel -> navigate(LoginFragmentDirections.actionLoginFragmentToParentHomeFragment())
                                is ProfessorModel -> TODO()
                                else -> throw IllegalStateException("The user model type is unknown")
                            }
                        }
                        onError {
                            val msg: String? = it.apiCallErrorResponse?.message
                            InfoAlert().apply {
                                textMessage = MessageFormat.from(msg ?: R.string.error_occurred)
                                borderColor = R.color.colorRed
                            }.showSuspendAlert()
                        }
                    }
                }
                onError {
                    hideLoadAlert()
                    val msg: String? = it.apiCallErrorResponse?.message
                    InfoAlert().apply {
                        textMessage = MessageFormat.from(msg ?: R.string.error_occurred)
                        borderColor = R.color.colorRed
                    }.showSuspendAlert()
                }
            }
        }
    }

    fun passwordForgotten() = viewModelScope.launch {
        navigate(LoginFragmentDirections.actionLoginFragmentToPasswordForgottenFragment())
    }

    fun signUp() = viewModelScope.launch {
        navigate(LoginFragmentDirections.actionLoginFragmentToChooseCreateAccountFragment())
    }

}