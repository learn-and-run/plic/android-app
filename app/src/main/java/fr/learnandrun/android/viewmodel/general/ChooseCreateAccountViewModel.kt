package fr.learnandrun.android.viewmodel.general

import androidx.lifecycle.viewModelScope
import fr.learnandrun.android.view.ui.fragment.general.ChooseCreateAccountFragmentDirections
import fr.learnandrun.android.viewmodel.BasicViewModel
import kotlinx.coroutines.launch

class ChooseCreateAccountViewModel : BasicViewModel() {

    fun chooseStudent() = viewModelScope.launch {
        navigate(
            ChooseCreateAccountFragmentDirections
                .actionChooseCreateAccountFragmentToCreateAccountStudentFragment()
        )
    }

    fun chooseParent() = viewModelScope.launch {
        navigate(
            ChooseCreateAccountFragmentDirections
                .actionChooseCreateAccountFragmentToCreateAccountParentFragment()
        )
    }

}