package fr.learnandrun.android.viewmodel.student

import androidx.lifecycle.viewModelScope
import fr.learnandrun.android.R
import fr.learnandrun.android.business.CurrentUser
import fr.learnandrun.android.service.ScoreService
import fr.learnandrun.android.service.UserService
import fr.learnandrun.android.service.model.score.ScoreModel
import fr.learnandrun.android.service.model.user.ModuleType
import fr.learnandrun.android.service.model.user.StudentModel
import fr.learnandrun.android.tools.delegate.DelegateLiveData
import fr.learnandrun.android.tools.delegate.DelegateNullableLiveData
import fr.learnandrun.android.view.ui.fragment.student.StudentHomeFragmentDirections
import fr.learnandrun.android.viewmodel.BasicViewModel
import fr.learnandrun.android.viewmodel.alert.InfoAlert
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class StudentHomeViewModel(
    val currentUser: CurrentUser,
    val userService: UserService,
    val scoreService: ScoreService
) : BasicViewModel() {

    val studentModel: StudentModel
        get() = currentUser.userModel as? StudentModel ?: throw IllegalStateException("Current user must be a student")

    fun parents() {
        viewModelScope.launch {
            navigate(StudentHomeFragmentDirections.actionStudentHomeFragmentToStudentParentsFragment())
        }
    }

    fun friends() {
        viewModelScope.launch {
            navigate(StudentHomeFragmentDirections.actionStudentHomeFragmentToStudentFriendsFragment())
        }
    }

    fun invitations() {
        viewModelScope.launch {
            navigate(StudentHomeFragmentDirections.actionStudentHomeFragmentToInvitationsFragment())
        }
    }

    fun settings() {
        viewModelScope.launch {
            navigate(StudentHomeFragmentDirections.actionStudentHomeFragmentToStudentParametersFragment())
        }
    }

    var selectedModule = ModuleType.GENERAL
    val modules: List<ModuleType> = ModuleType.values().toList()
    val listScoresLive = DelegateNullableLiveData<List<ScoreModel>>()
    var listScores by listScoresLive

    val isRefreshingLive = DelegateLiveData(false)
    var isRefreshing by isRefreshingLive

    init {
        viewModelScope.launch {
            delay(1)
            refreshList()
        }
    }

    suspend fun refreshList() {
        isRefreshing = true
        scoreService.getScoresOfPseudoByModule(
            studentModel.pseudo,
            selectedModule
        ) {
            onSuccess {
                listScores = it.scoreModels
            }
            onError {
                InfoAlert().apply {
                    textId = R.string.error_occurred
                }.showSuspendAlert()
            }
        }
        isRefreshing = false
    }

    suspend fun moduleClick(moduleType: ModuleType) {
        selectedModule = moduleType
        refreshList()
    }


}
