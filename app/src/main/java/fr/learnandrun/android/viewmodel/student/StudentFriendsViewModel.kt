package fr.learnandrun.android.viewmodel.student

import androidx.lifecycle.viewModelScope
import fr.learnandrun.android.R
import fr.learnandrun.android.service.UserService
import fr.learnandrun.android.service.model.user.RelationModel
import fr.learnandrun.android.service.model.user.StudentModel
import fr.learnandrun.android.service.model.user.UserType
import fr.learnandrun.android.tools.MessageFormat
import fr.learnandrun.android.tools.delegate.DelegateLiveData
import fr.learnandrun.android.tools.delegate.DelegateNullableLiveData
import fr.learnandrun.android.view.ui.fragment.student.StudentFriendsFragmentDirections
import fr.learnandrun.android.viewmodel.BasicViewModel
import fr.learnandrun.android.viewmodel.alert.InfoAlert
import fr.learnandrun.android.viewmodel.alert.InputAlert
import kotlinx.coroutines.launch

class StudentFriendsViewModel(
    val userService: UserService
) : BasicViewModel() {

    val refreshingLive = DelegateLiveData(false)
    var refreshing by refreshingLive

    val listStudentRelationsLive = DelegateNullableLiveData<List<RelationModel>>()
    var listStudentRelations by listStudentRelationsLive

    fun back() {
        viewModelScope.launch {
            navigateBack()
        }
    }

    fun ladder() {
        viewModelScope.launch {
            navigate(StudentFriendsFragmentDirections.actionStudentFriendsFragmentToStudentLadderFragment())
        }
    }

    init {
        viewModelScope.launch {
            refreshStudents()
        }
    }

    suspend fun refreshStudents() {
        refreshing = true
        userService.getAllRelations(UserType.STUDENT) {
            minimumDelay = 500
            onSuccess {
                listStudentRelations = it
            }
            onError {
                InfoAlert().apply {
                    textId = R.string.error_occurred
                }.showSuspendAlert()
            }
        }
        refreshing = false
    }

    fun studentTrash(studentRelation: RelationModel) {
        viewModelScope.launch {
            refreshing = true
            userService.deleteRelation(studentRelation.id) {
                onSuccess {
                    refreshStudents()
                }
                onError {
                    InfoAlert().apply {
                        textMessage = MessageFormat(it.message ?: R.string.error_occurred)
                    }.showSuspendAlert()
                }
            }
            refreshing = false
        }
    }

    fun studentClick(studentModel: StudentModel) {
        viewModelScope.launch {
            navigate(
                StudentFriendsFragmentDirections.actionStudentFriendsFragmentToStatsStudentFragment(studentModel.pseudo)
            )
        }
    }

    fun addStudent() {
        viewModelScope.launch {
            val username: String = InputAlert().apply {
                hintTextId = R.string.id
                textId = R.string.input_add_child
            }.showSuspendAlert() ?: return@launch
            showLoadAlert()
            userService.addRelation(username) {
                onPreHandle {
                    hideLoadAlert()
                }
                onSuccess {
                    InfoAlert().apply {
                        textId = R.string.invitation_sent
                    }.showSuspendAlert()
                }
                onError {
                    InfoAlert().apply {
                        textMessage = MessageFormat(it.message ?: R.string.error_occurred)
                    }.showSuspendAlert()
                }
            }
        }
    }

}