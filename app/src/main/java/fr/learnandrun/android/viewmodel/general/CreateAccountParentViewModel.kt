package fr.learnandrun.android.viewmodel.general

import androidx.lifecycle.viewModelScope
import fr.learnandrun.android.R
import fr.learnandrun.android.service.UserService
import fr.learnandrun.android.tools.MessageFormat
import fr.learnandrun.android.tools.delegate.DelegateLiveData
import fr.learnandrun.android.tools.delegate.DelegateLiveText
import fr.learnandrun.android.view.ui.fragment.general.CreateAccountParentFragmentDirections
import fr.learnandrun.android.viewmodel.BasicViewModel
import fr.learnandrun.android.viewmodel.alert.InfoAlert
import kotlinx.coroutines.launch

class CreateAccountParentViewModel(private val userService: UserService) : BasicViewModel() {

    val emailLiveText: DelegateLiveText = DelegateLiveText {
        createIsActive = allFieldsAreFilled()
    }
    val lastnameLiveText: DelegateLiveText = DelegateLiveText {
        createIsActive = allFieldsAreFilled()
    }
    val firstnameLiveText: DelegateLiveText = DelegateLiveText {
        createIsActive = allFieldsAreFilled()
    }
    val pseudoLiveText: DelegateLiveText = DelegateLiveText {
        createIsActive = allFieldsAreFilled()
    }
    val passwordLiveText: DelegateLiveText = DelegateLiveText {
        createIsActive = allFieldsAreFilled()
    }

    val createIsActiveLive = DelegateLiveData(false)
    var createIsActive by createIsActiveLive

    var email by emailLiveText
    var lastname by lastnameLiveText
    var firstname by firstnameLiveText
    var pseudo by pseudoLiveText
    var password by passwordLiveText

    private fun allFieldsAreFilled(): Boolean {
        return listOf(email, lastname, firstname, pseudo, password).all { it.isNotBlank() }
    }

    fun create() {
        if (!allFieldsAreFilled()) return
        viewModelScope.launch {
            showLoadAlert()
            userService.createParent(pseudo, firstname, lastname, password, email) {
                minimumDelay = 1000
                onPreHandle {
                    hideLoadAlert()
                }
                onSuccess {
                    InfoAlert().apply {
                        textId = R.string.account_created
                    }.showSuspendAlert()
                    navigate(
                        CreateAccountParentFragmentDirections
                            .actionCreateAccountParentFragmentToLoginFragment()
                    )
                }
                onError {
                    InfoAlert().apply {
                        textMessage = MessageFormat.from(it.message ?: R.string.error_occurred)
                    }.showSuspendAlert()
                }
            }
        }

    }

    fun back() = viewModelScope.launch {
        navigateBack()
    }

}
