@file:Suppress("EXPERIMENTAL_API_USAGE")

package fr.learnandrun.android.business

import fr.learnandrun.android.business.cookie.FileCookieStorage
import io.ktor.client.*
import io.ktor.client.engine.cio.*
import io.ktor.client.features.*
import io.ktor.client.features.cookies.*
import io.ktor.client.features.json.*
import io.ktor.client.features.json.serializer.*
import kotlinx.serialization.json.Json
import java.io.File

class KtorClient(val url: String, cookieStorageFile: File) {
    val client = HttpClient(CIO) {
        install(JsonFeature) {
            serializer = KotlinxSerializer(json)
        }
        install(HttpCookies) {
            // Will keep an in-memory map with all the cookies from previous requests.
            storage = FileCookieStorage(cookieStorageFile)
        }
        install(HttpTimeout) {
            // timeout config
            requestTimeoutMillis = 10_000//10s
        }
    }

    companion object {
        val json = Json {
            prettyPrint = true
            isLenient = true
            ignoreUnknownKeys = true
        }
    }
}