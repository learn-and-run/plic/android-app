package fr.learnandrun.android.business.cookie

import io.ktor.client.features.cookies.*
import io.ktor.http.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import java.io.File
import java.nio.charset.StandardCharsets

class FileCookieStorage(
    val file: File
) : CookiesStorage {

    val acceptAllCookiesStorage = AcceptAllCookiesStorage()

    val container: MutableMap<String, MutableList<SimpleCookie>> = if (file.exists())
        Json.decodeFromString(file.readText(StandardCharsets.UTF_8))
    else
        mutableMapOf()

    init {
        GlobalScope.launch(Dispatchers.IO) {
            for ((requestUrlString, simpleCookies) in container) {
                for (simpleCookie in simpleCookies) {
                    acceptAllCookiesStorage.addCookie(Url(requestUrlString), simpleCookie.asCookie())
                }
            }
        }
    }

    private fun addToFile(requestUrl: Url, cookie: Cookie) {
        val requestUrlString: String = with(requestUrl) {
            buildString {
                append(protocol.name)
                append("://")
                append(authority)
            }
        }
        if (!container.containsKey(requestUrlString))
            container[requestUrlString] = mutableListOf()
        container[requestUrlString]?.removeAll { it.name == cookie.name }
        container[requestUrlString]?.add(SimpleCookie.fromCookie(cookie))
        file.writeText(Json.encodeToString(container), StandardCharsets.UTF_8)
    }

    override suspend fun addCookie(requestUrl: Url, cookie: Cookie) {
        addToFile(requestUrl, cookie)
        acceptAllCookiesStorage.addCookie(requestUrl, cookie)
    }

    override fun close() {
        acceptAllCookiesStorage.close()
    }

    override suspend fun get(requestUrl: Url): List<Cookie> {
        return acceptAllCookiesStorage.get(requestUrl)
    }

}