package fr.learnandrun.android.business

import fr.learnandrun.android.service.model.user.UserModel

class CurrentUser {

    private var _userModel: UserModel? = null
    var userModel: UserModel
        get() = _userModel ?: throw IllegalStateException("The user must not be null!")
        set(value) {
            _userModel = value
        }


}