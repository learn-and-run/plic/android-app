package fr.learnandrun.android.business.cookie

import io.ktor.http.*
import io.ktor.util.date.*
import kotlinx.serialization.Serializable

@Serializable
data class SimpleCookie(
    val name: String,
    val value: String,
    val encoding: CookieEncoding = CookieEncoding.URI_ENCODING,
    val maxAge: Int = 0,
    val timestamp: Long? = null,
    val domain: String? = null,
    val path: String? = null,
    val secure: Boolean = false,
    val httpOnly: Boolean = false,
    val extensions: Map<String, String?> = emptyMap()
) {

    fun asCookie(): Cookie = Cookie(
        name,
        value,
        encoding,
        maxAge,
        timestamp?.let { GMTDate(timestamp) },
        domain,
        path,
        secure,
        httpOnly,
        extensions
    )

    companion object {
        fun fromCookie(cookie: Cookie): SimpleCookie = with(cookie) {
            SimpleCookie(
                name,
                value,
                encoding,
                maxAge,
                expires?.timestamp,
                domain,
                path,
                secure,
                httpOnly,
                extensions
            )
        }
    }
}