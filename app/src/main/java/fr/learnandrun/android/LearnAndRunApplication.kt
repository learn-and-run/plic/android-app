package fr.learnandrun.android

import android.app.Application
import fr.learnandrun.android.business.CurrentUser
import fr.learnandrun.android.business.KtorClient
import fr.learnandrun.android.repository.score.ScoreRepository
import fr.learnandrun.android.repository.user.UserRepository
import fr.learnandrun.android.service.ScoreService
import fr.learnandrun.android.service.UserService
import fr.learnandrun.android.viewmodel.general.*
import fr.learnandrun.android.viewmodel.parent.ParentHomeViewModel
import fr.learnandrun.android.viewmodel.parent.ParentParametersViewModel
import fr.learnandrun.android.viewmodel.student.*
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.dsl.module
import java.io.File

class LearnAndRunApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        val ktorModule = module {
            single { KtorClient(getString(R.string.back_end_url), File(filesDir, "cookieStorage.json")) }
            single { UserRepository(get()) }
            single { ScoreRepository(get()) }
            single { UserService(get()) }
            single { ScoreService(get()) }
            single { CurrentUser() }
        }
        val viewModelModule = module {
            viewModel { WaitingViewModel(get(), get()) }
            viewModel { LoginViewModel(get(), get()) }
            viewModel { PasswordForgottenViewModel(get()) }
            viewModel { StudentHomeViewModel(get(), get(), get()) }
            viewModel { ParentHomeViewModel(get(), get()) }
            viewModel { ChooseCreateAccountViewModel() }
            viewModel { CreateAccountParentViewModel(get()) }
            viewModel { CreateAccountStudentViewModel(get()) }
            viewModel { StudentParametersViewModel(get(), get()) }
            viewModel { ParentParametersViewModel(get(), get()) }
            viewModel { StatsStudentViewModel(get(), get(), get()) }
            viewModel { InvitationsViewModel(get()) }
            viewModel { StudentParentsViewModel(get()) }
            viewModel { StudentFriendsViewModel(get()) }
            viewModel { StudentLadderViewModel(get(), get(), get()) }
        }
        startKoin {
            androidContext(this@LearnAndRunApplication)
            modules(ktorModule)
            modules(viewModelModule)
        }
    }

    override fun onTerminate() {
        super.onTerminate()
        stopKoin()

    }
}