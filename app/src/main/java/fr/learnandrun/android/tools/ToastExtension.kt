package fr.learnandrun.android.tools

import android.content.Context
import android.widget.Toast
import androidx.fragment.app.Fragment

/**
 * Extension method that simplify the use of toast (short toast)
 * @param msg the message to display
 */
fun Context.toast(msg: String) =
    Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()

/**
 * Extension method that simplify the use of toast (long toast)
 * @param msg the message to display
 */
fun Context.toastLong(msg: String) =
    Toast.makeText(this, msg, Toast.LENGTH_LONG).show()


fun Fragment.toast(msg: String) = requireContext().toast(msg)
fun Fragment.toastLong(msg: String) = requireContext().toastLong(msg)