package fr.learnandrun.android.tools.converter

import fr.learnandrun.android.repository.user.dto.CharacterDto
import fr.learnandrun.android.repository.user.dto.UserDto
import fr.learnandrun.android.repository.user.dto.response.AllRelationsDtoResponse
import fr.learnandrun.android.repository.user.dto.response.PendingInvitationDto
import fr.learnandrun.android.repository.user.dto.response.PendingInvitationsDtoResponse
import fr.learnandrun.android.service.model.user.*

fun CharacterDto.toCharacter(): CharacterModel = CharacterModel(id, name)
fun UserDto.Student.toStudent(): StudentModel = StudentModel(
    pseudo,
    firstname,
    lastname,
    email,
    character.toCharacter(),
    levelType
)

fun UserDto.Parent.toParent(): ParentModel = ParentModel(
    pseudo,
    firstname,
    lastname,
    email,
    character.toCharacter()
)

fun UserDto.Professor.toProfessor(): ProfessorModel = ProfessorModel(
    pseudo,
    firstname,
    lastname,
    email,
    character.toCharacter()
)

fun UserDto.toUser(): UserModel = when (this) {
    is UserDto.Student -> toStudent()
    is UserDto.Parent -> toParent()
    is UserDto.Professor -> toProfessor()
}

fun AllRelationsDtoResponse.toRelationModels(): List<RelationModel> = relations.map {
    RelationModel(it.id, it.user.toUser())
}

fun PendingInvitationDto.toInvitationModel(): InvitationModel = InvitationModel(id, user.pseudo)

fun PendingInvitationsDtoResponse.toListInvitationModels(): List<InvitationModel> =
    pendingInvitations.map {
        it.toInvitationModel()
    }