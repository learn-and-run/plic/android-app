package fr.learnandrun.android.tools.handler

fun interface SetupApiCall<ReturnType : Any> {
    fun ApiCallHandler<ReturnType>.setup()
}