package fr.learnandrun.android.tools

import fr.learnandrun.android.service.error.ApiCallErrorException

suspend fun <T> tryCallOrExecute(
    function: suspend () -> T,
    elseCall: suspend (ApiCallErrorException) -> Unit
): T? =
    try {
        function()
    } catch (e: ApiCallErrorException) {
        null.also { elseCall(e) }
    }