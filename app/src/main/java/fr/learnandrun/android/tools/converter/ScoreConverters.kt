package fr.learnandrun.android.tools.converter

import fr.learnandrun.android.repository.score.dto.response.CircuitScoreDtoResponse
import fr.learnandrun.android.repository.score.dto.response.RankingDtoResponse
import fr.learnandrun.android.repository.score.dto.response.ScoreDto
import fr.learnandrun.android.repository.score.dto.response.ScoresOfModuleDtoResponse
import fr.learnandrun.android.service.model.score.CircuitScoreModel
import fr.learnandrun.android.service.model.score.ScoreModel
import fr.learnandrun.android.service.model.score.ScoresByModuleModel
import fr.learnandrun.android.service.model.user.LadderModel

fun ScoresOfModuleDtoResponse.toScoresByModule(): ScoresByModuleModel =
    ScoresByModuleModel(scores.map { it.toScore() })

fun ScoreDto.toScore(): ScoreModel =
    ScoreModel(circuitName, moduleType, score, spendTime, bestTime)

fun CircuitScoreDtoResponse.toCircuitScore(): CircuitScoreModel =
    CircuitScoreModel(circuitId, bestScore, currentScore, spendTime, bestTime)

fun RankingDtoResponse.toLadderList(): List<LadderModel> = friends.map {
    LadderModel(it.ladder, it.pseudo, it.average)
}