package fr.learnandrun.android.tools.handler

import fr.learnandrun.android.tools.minimumDelay

open class SuccessOrCancelHandler<ReturnType : Any> {

    var minimumDelay = 0
    var catchError = true

    open var onSuccessFunction: (suspend (response: ReturnType) -> Unit)? = null
    open fun onSuccess(onSuccessFunction: suspend (response: ReturnType) -> Unit) {
        this.onSuccessFunction = onSuccessFunction
    }

    open var onCancelFunction: (suspend () -> Unit)? = null
    open fun onCancel(onCancelFunction: suspend () -> Unit) {
        this.onCancelFunction = onCancelFunction
    }

    open var onPreHandleFunction: (suspend () -> Unit)? = null
    open fun onPreHandle(onPreHandleFunction: suspend () -> Unit) {
        this.onPreHandleFunction = onPreHandleFunction
    }

    companion object {

        suspend fun <ReturnType : Any> handleFunction(
            setupInterface: SetupSuccessOrCancel<ReturnType>,
            function: suspend () -> ReturnType?
        ): ReturnType? {

            val handler = SuccessOrCancelHandler<ReturnType>()
            handler.catchError = false
            setupInterface.run {
                handler.setup()
            }

            val result: ReturnType? = minimumDelay(handler.minimumDelay) { function() }

            handler.onPreHandleFunction?.invoke()
            when {
                result != null -> handler.onSuccessFunction?.invoke(result)
                else -> handler.onCancelFunction?.invoke()
            }
            return result
        }

        suspend inline fun <reified ReturnType : Any>
                (suspend () -> ReturnType?).handle(
            setupInterface: SetupSuccessOrCancel<ReturnType>
        ): ReturnType? = handleFunction(setupInterface, this)
    }

}