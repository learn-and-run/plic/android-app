package fr.learnandrun.android.tools

import android.content.Context

data class MessageFormat(
    val format: Any,
    val args: List<Any> = listOf()
) {

    private data class Id(
        val id: Int
    )

    fun getMessage(context: Context): String {
        return String.format(
            if (format is Int)
                context.getString(format)
            else format.toString(),
            *args.map {
                when (it) {
                    is Id -> context.getString(it.id)
                    else -> it
                }
            }.toTypedArray()
        )
    }

    companion object {
        fun from(format: Any, vararg args: Any) = MessageFormat(format, args.toList())
    }
}