package fr.learnandrun.android.tools.handler

import fr.learnandrun.android.tools.minimumDelay

open class SuccessOrCancelOrErrorHandler<ReturnType : Any>
    : SuccessOrCancelHandler<ReturnType>() {

    open var onErrorFunction: (suspend (error: Throwable) -> Unit)? = null
    open fun onError(onErrorFunction: suspend (error: Throwable) -> Unit) {
        this.onErrorFunction = onErrorFunction
    }

    companion object {
        suspend inline fun <ReturnType : Any>
                handleFunctionWithError(
            setupInterface: SetupSuccessOrCancelOrError<ReturnType>,
            crossinline function: suspend () -> ReturnType?
        ): HandlerResult<ReturnType, Throwable> {

            val handler = SuccessOrCancelOrErrorHandler<ReturnType>()
            setupInterface.run {
                handler.setup()
            }

            val (result: ReturnType?, error: Throwable?) = minimumDelay(handler.minimumDelay) {
                if (handler.catchError)
                    try {
                        function() to null
                    } catch (e: Throwable) {
                        null to e
                    }
                else
                    function() to null
            }
            handler.onPreHandleFunction?.invoke()
            return when {
                result != null -> {
                    handler.onSuccessFunction?.invoke(result)
                    HandlerResult(ResultType.SUCCESS, result, null)
                }
                error != null -> {
                    handler.onErrorFunction?.invoke(error)
                    HandlerResult(ResultType.ERROR, null, error)
                }
                else -> {
                    handler.onCancelFunction?.invoke()
                    HandlerResult(ResultType.CANCEL, null, null)
                }
            }
        }

        suspend inline fun <reified ReturnType : Any>
                (suspend () -> ReturnType?).handleWithError(
            setupInterface: SetupSuccessOrCancelOrError<ReturnType>,
        ): HandlerResult<ReturnType, Throwable> =
            handleFunctionWithError(setupInterface, this)
    }
}