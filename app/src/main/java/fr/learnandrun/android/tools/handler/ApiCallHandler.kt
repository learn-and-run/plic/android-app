package fr.learnandrun.android.tools.handler

import android.util.Log
import fr.learnandrun.android.business.KtorClient
import fr.learnandrun.android.service.error.ApiCallErrorException
import fr.learnandrun.android.service.error.ApiCallErrorResponse
import fr.learnandrun.android.tools.minimumDelay
import io.ktor.client.features.*
import io.ktor.client.statement.*
import kotlinx.serialization.decodeFromString

open class ApiCallHandler<ReturnType : Any>
    : SuccessOrCancelHandler<ReturnType>() {

    open var onErrorFunction: (suspend (error: ApiCallErrorException) -> Unit)? = null
    open fun onError(onErrorFunction: suspend (error: ApiCallErrorException) -> Unit) {
        this.onErrorFunction = onErrorFunction
    }

    companion object {
        suspend fun <ReturnType : Any> handleFunctionCall(
            setupInterface: SetupApiCall<ReturnType>,
            function: suspend () -> ReturnType?
        ): HandlerResult<ReturnType, ApiCallErrorException> {

            val handler = ApiCallHandler<ReturnType>()
            setupInterface.run {
                handler.setup()
            }

            val (result: ReturnType?, error: ApiCallErrorException?) =
                minimumDelay(handler.minimumDelay) {
                    try {
                        function() to null
                    } catch (throwable: Throwable) {
                        val httpResponse = when (throwable) {
                            is RedirectResponseException -> throwable.response
                            is ClientRequestException -> throwable.response
                            is ServerResponseException -> throwable.response
                            is ResponseException -> throwable.response
                            else -> null
                        }
                        val status = httpResponse?.status
                        val response = httpResponse?.let {
                            try {
                                KtorClient.json.decodeFromString<ApiCallErrorResponse>(
                                    String(it.readBytes())
                                )
                            } catch (t: Throwable) {
                                Log.d("Decode Error", "Error while decoding response")
                                Log.d("Decode Error", t.message ?: "No message")
                                Log.d("Decode Error", t.stackTraceToString())
                                null
                            }
                        }
                        null to ApiCallErrorException(status, response, throwable)
                    }
                }
            handler.onPreHandleFunction?.invoke()
            return when {
                result != null -> {
                    handler.onSuccessFunction?.invoke(result)
                    HandlerResult(ResultType.SUCCESS, result, null)
                }
                error != null -> {
                    handler.onErrorFunction?.invoke(error)
                    HandlerResult(ResultType.ERROR, null, error)
                }
                else -> {
                    handler.onCancelFunction?.invoke()
                    HandlerResult(ResultType.CANCEL, null, null)
                }
            }
        }

        suspend fun <ReturnType : Any> (suspend () -> ReturnType?).handleCall(
            setupInterface: SetupApiCall<ReturnType>,
        ): HandlerResult<ReturnType, ApiCallErrorException> =
            handleFunctionCall(setupInterface, this)
    }
}