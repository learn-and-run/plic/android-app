package fr.learnandrun.android.tools.handler

import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KProperty

data class HandlerResult<ReturnType : Any, ErrorType : Throwable>(
    val resultType: ResultType,
    val result: ReturnType?,
    val error: ErrorType?
) : ReadOnlyProperty<Any?, ReturnType?> {
    override fun getValue(thisRef: Any?, property: KProperty<*>): ReturnType? = result
}