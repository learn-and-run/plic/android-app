package fr.learnandrun.android.tools.delegate

import android.text.Editable
import android.widget.EditText
import androidx.core.widget.doAfterTextChanged
import androidx.lifecycle.LifecycleOwner

/**
 * A [DelegateLiveText] is a [DelegateLiveData] of String
 * that has util functions to be linked to an edit text.
 * It can be linked as usual with live data (from live data to edit text).
 * Or it can be linked from the edit text to the live data.
 * Or it can be linked in both ways!
 *
 * @param textChangedFromView permits to do some business logic when the text change from view
 */
class DelegateLiveText(
    initialValue: String = "",
    val textChangedFromView: (newText: String) -> Unit = { }
) : DelegateLiveData<String>(initialValue) {

    /**
     * Update live data from edit text
     */
    fun listenToEditTextView(editText: EditText) {
        editText.doAfterTextChanged { editable: Editable? ->
            editable?.let {
                value = it.toString()
                textChangedFromView(value)
            }
        }
    }

    /**
     * Update edit text from live data
     */
    fun observeEditText(owner: LifecycleOwner, editText: EditText) = observe(owner) {
        if (editText.text.toString() != it)
            editText.setText(it)
    }

    /**
     * Bind (two ways) edit text and live data
     */
    fun bindEditTextView(owner: LifecycleOwner, editText: EditText) {
        observeEditText(owner, editText)
        listenToEditTextView(editText)
    }

}