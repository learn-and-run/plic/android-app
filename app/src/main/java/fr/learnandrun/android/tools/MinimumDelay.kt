package fr.learnandrun.android.tools

import kotlinx.coroutines.delay

data class DurationResult<T>(
    val duration: Long,
    val result: T
)

inline fun <T> measureDuration(block: () -> T): DurationResult<T> {
    val start = System.currentTimeMillis()
    val result = block()
    return DurationResult(System.currentTimeMillis() - start, result)
}

suspend inline fun delayToCatchUp(duration: Long, delayToCatchUp: Int) {
    if (duration < delayToCatchUp)
        delay(delayToCatchUp - duration)
}

suspend inline fun <THIS, RESULT_TYPE> THIS.minimumDelay(
    minimumDelayMilli: Int,
    crossinline function: suspend THIS.() -> RESULT_TYPE
): RESULT_TYPE =
    measureDuration { function() }
        .apply { delayToCatchUp(duration, minimumDelayMilli) }
        .result

suspend inline fun <THIS, RESULT_TYPE> THIS.doWhileWithDelayUntilRetry(
    delayUntilRetry: Int,
    crossinline doFunction: suspend THIS.(nbTry: Int) -> RESULT_TYPE,
    crossinline whilePredicate: suspend THIS.(RESULT_TYPE) -> Boolean
): RESULT_TYPE {
    var nbTry = 1
    do {
        val (duration, result) = measureDuration { doFunction(nbTry++) }
        if (whilePredicate(result))
            delayToCatchUp(duration, delayUntilRetry)
        else
            return result
    } while (true)
}

suspend inline fun <THIS, RESULT_TYPE> THIS.doWhileNullWithDelayUntilRetry(
    delayUntilRetry: Int,
    crossinline doFunction: suspend THIS.(nbTry: Int) -> RESULT_TYPE?
): RESULT_TYPE {
    var nbTry = 1
    do {
        val (duration, result) = measureDuration { doFunction(nbTry++) }
        if (result != null)
            return result
        delayToCatchUp(duration, delayUntilRetry)
    } while (true)
}