package fr.learnandrun.android.tools.handler

fun interface SetupSuccessOrCancelOrError<ReturnType : Any> {
    fun SuccessOrCancelOrErrorHandler<ReturnType>.setup()
}