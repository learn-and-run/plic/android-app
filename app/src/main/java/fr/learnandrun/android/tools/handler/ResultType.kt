package fr.learnandrun.android.tools.handler

enum class ResultType {
    SUCCESS,
    ERROR,
    CANCEL
}