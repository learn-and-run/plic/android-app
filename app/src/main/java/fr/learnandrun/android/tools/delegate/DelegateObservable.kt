package fr.learnandrun.android.tools.delegate

import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

class DelegateObservable<T>(
    initialValue: T? = null
) : ReadWriteProperty<Any?, T> {

    private var _value = initialValue

    var observer: ((value: T) -> Unit)? = null

    fun isInitialized(): Boolean = _value != null

    fun getValue(): T = _value ?: throw NullPointerException("The value has not been initialized!")

    fun setValue(value: T) {
        if (_value != value) {
            _value = value
            observer?.invoke(value)
        }
    }

    override fun getValue(thisRef: Any?, property: KProperty<*>): T = getValue()
    override fun setValue(thisRef: Any?, property: KProperty<*>, value: T) = setValue(value)

}