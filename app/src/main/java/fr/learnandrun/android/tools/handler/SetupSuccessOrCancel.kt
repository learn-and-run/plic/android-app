package fr.learnandrun.android.tools.handler

fun interface SetupSuccessOrCancel<ReturnType : Any> {
    fun SuccessOrCancelHandler<ReturnType>.setup()
}