package fr.learnandrun.android.repository.user.dto.request

import kotlinx.serialization.Serializable

@Serializable
data class DeleteRelationDtoRequest(
    val relationId: Long
)