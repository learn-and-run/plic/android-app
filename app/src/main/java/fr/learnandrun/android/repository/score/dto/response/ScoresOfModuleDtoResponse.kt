package fr.learnandrun.android.repository.score.dto.response

import kotlinx.serialization.Serializable

@Serializable
data class ScoresOfModuleDtoResponse(
    val scores: List<ScoreDto>
)