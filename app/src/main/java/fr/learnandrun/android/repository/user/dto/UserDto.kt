package fr.learnandrun.android.repository.user.dto

import fr.learnandrun.android.service.model.user.LevelType
import fr.learnandrun.android.service.model.user.UserType
import kotlinx.serialization.Serializable

@Serializable(UserDtoSerializer::class)
sealed class UserDto {
    @Serializable
    data class Student(
        val pseudo: String,
        val firstname: String,
        val lastname: String,
        val email: String,
        val userType: UserType,
        val character: CharacterDto,
        val levelType: LevelType
    ) : UserDto()

    @Serializable
    data class Parent(
        val pseudo: String,
        val firstname: String,
        val lastname: String,
        val email: String,
        val userType: UserType,
        val character: CharacterDto
    ) : UserDto()

    @Serializable
    data class Professor(
        val pseudo: String,
        val firstname: String,
        val lastname: String,
        val email: String,
        val userType: UserType,
        val character: CharacterDto
    ) : UserDto()
}

