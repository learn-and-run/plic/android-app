package fr.learnandrun.android.repository.user.dto.request

import fr.learnandrun.android.service.model.user.UserType
import kotlinx.serialization.Serializable

@Serializable
data class AllRelationsDtoRequest(
    val userType: UserType
)