package fr.learnandrun.android.repository.user.dto

import kotlinx.serialization.Serializable

@Serializable
data class CharacterDto(
    val id: Long,
    val name: String
)