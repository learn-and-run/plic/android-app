package fr.learnandrun.android.repository.user.dto.request

import fr.learnandrun.android.service.model.user.LevelType
import kotlinx.serialization.Serializable

@Serializable
data class UpdateStudentDtoRequest(
    val pseudo: String,
    val firstname: String,
    val lastname: String,
    val levelType: LevelType
)