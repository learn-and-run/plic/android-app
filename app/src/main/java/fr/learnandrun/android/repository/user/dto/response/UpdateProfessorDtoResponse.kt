package fr.learnandrun.android.repository.user.dto.response

import kotlinx.serialization.Serializable

@Serializable
data class UpdateProfessorDtoResponse(
    val pseudo: String,
    val firstname: String,
    val lastname: String
)