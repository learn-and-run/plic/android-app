package fr.learnandrun.android.repository.user.dto.request

import kotlinx.serialization.Serializable

@Serializable
data class CreateParentDtoRequest(
    val pseudo: String,
    val firstname: String,
    val lastname: String,
    val password: String,
    val email: String
)