package fr.learnandrun.android.repository.score.dto.request

import fr.learnandrun.android.service.model.user.ModuleType
import kotlinx.serialization.Serializable

@Serializable
data class RankingDtoRequest(
    val moduleType: ModuleType
)