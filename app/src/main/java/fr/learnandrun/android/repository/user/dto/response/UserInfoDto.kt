package fr.learnandrun.android.repository.user.dto.response

import fr.learnandrun.android.repository.user.dto.CharacterDto
import fr.learnandrun.android.service.model.user.UserType
import kotlinx.serialization.Serializable

@Serializable
data class UserInfoDto(
    val pseudo: String,
    val firstname: String,
    val lastname: String,
    val email: String,
    val userType: UserType,
    val character: CharacterDto
)