package fr.learnandrun.android.repository.score.dto.response

import fr.learnandrun.android.service.model.user.ModuleType
import kotlinx.serialization.Serializable

@Serializable
data class RankingDtoResponse(
    val friends: List<FriendDto>,
    val moduleType: ModuleType
) {
    @Serializable
    data class FriendDto(
        val ladder: Int,
        val pseudo: String,
        val average: Int,
        val circuits: List<CircuitDto>
    ) {
        @Serializable
        data class CircuitDto(
            val name: String,
            val bestScore: Int,
            val spendTime: Int,
            val bestTime: Int
        )
    }
}