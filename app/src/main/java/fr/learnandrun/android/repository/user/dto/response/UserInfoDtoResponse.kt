package fr.learnandrun.android.repository.user.dto.response

import fr.learnandrun.android.repository.user.dto.UserDto
import fr.learnandrun.android.service.model.user.UserType
import kotlinx.serialization.Serializable

@Serializable
data class UserInfoDtoResponse(
    val userType: UserType,
    val user: UserDto
)