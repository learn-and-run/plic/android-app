package fr.learnandrun.android.repository.score.dto.request

import kotlinx.serialization.Serializable

@Serializable
data class CircuitScoreDtoRequest(
        val pseudo: String,
        val circuitName: String
)