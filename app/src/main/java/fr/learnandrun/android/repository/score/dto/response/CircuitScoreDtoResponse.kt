package fr.learnandrun.android.repository.score.dto.response

import kotlinx.serialization.Serializable

@Serializable
data class CircuitScoreDtoResponse(
        val circuitId: Long,
        val bestScore: Int,
        val currentScore: Int,
        val spendTime: Int,
        val bestTime: Int
)