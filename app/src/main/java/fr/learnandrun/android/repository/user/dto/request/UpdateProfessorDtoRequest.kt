package fr.learnandrun.android.repository.user.dto.request

import kotlinx.serialization.Serializable

@Serializable
data class UpdateProfessorDtoRequest(
    val pseudo: String,
    val firstname: String,
    val lastname: String
)