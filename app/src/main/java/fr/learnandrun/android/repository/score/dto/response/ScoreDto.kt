package fr.learnandrun.android.repository.score.dto.response

import fr.learnandrun.android.service.model.user.ModuleType
import kotlinx.serialization.Serializable

@Serializable
data class ScoreDto(
    val circuitName: String,
    val moduleType: ModuleType,
    val score: Int,
    val spendTime: Int,
    val bestTime: Int
)
