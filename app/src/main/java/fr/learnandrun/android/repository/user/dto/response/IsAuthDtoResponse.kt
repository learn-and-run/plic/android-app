package fr.learnandrun.android.repository.user.dto.response

import kotlinx.serialization.Serializable

@Serializable
data class IsAuthDtoResponse(
    val isAuthenticated: Boolean
)