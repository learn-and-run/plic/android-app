package fr.learnandrun.android.repository.user.dto.response

import kotlinx.serialization.Serializable

@Serializable
data class IsFullyAuthDtoResponse(
    val isFullyAuthenticated: Boolean
)