package fr.learnandrun.android.repository.user.dto.response

import fr.learnandrun.android.repository.user.dto.UserDto
import fr.learnandrun.android.service.model.user.UserType
import kotlinx.serialization.Serializable

@Serializable
data class RelationDto(
    val id: Long,
    val user: UserDto,
    val userType: UserType
)