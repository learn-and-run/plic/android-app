package fr.learnandrun.android.repository.user.dto.response

import kotlinx.serialization.Serializable

@Serializable
data class CreateProfessorDtoResponse(
    val id: Long,
    val email: String,
    val pseudo: String,
    val firstname: String,
    val lastname: String
)