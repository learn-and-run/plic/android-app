package fr.learnandrun.android.repository.score

import fr.learnandrun.android.business.KtorClient
import fr.learnandrun.android.repository.Repository
import fr.learnandrun.android.repository.score.dto.request.CircuitScoreDtoRequest
import fr.learnandrun.android.repository.score.dto.request.RankingDtoRequest
import fr.learnandrun.android.repository.score.dto.request.ScoresOfPseudoByModuleDtoRequest
import fr.learnandrun.android.repository.score.dto.response.CircuitScoreDtoResponse
import fr.learnandrun.android.repository.score.dto.response.RankingDtoResponse
import fr.learnandrun.android.repository.score.dto.response.ScoresOfModuleDtoResponse
import io.ktor.client.request.*
import io.ktor.http.*

class ScoreRepository(
    ktorClient: KtorClient
) : Repository {

    private val client = ktorClient.client
    private val url = ktorClient.url

    suspend fun getScoresOfPseudoByModule(
        scoresOfPseudoByModuleDtoRequest: ScoresOfPseudoByModuleDtoRequest
    ): ScoresOfModuleDtoResponse = client.get("$url/api/score/module") {
        parameter("pseudo", scoresOfPseudoByModuleDtoRequest.pseudo)
        parameter("moduleType", scoresOfPseudoByModuleDtoRequest.moduleType)
    }

    suspend fun getCircuitScore(
        circuitScoreDtoRequest: CircuitScoreDtoRequest
    ): CircuitScoreDtoResponse = client.post("$url/api/score") {
        contentType(ContentType.Application.Json)
        body = circuitScoreDtoRequest
    }

    suspend fun getFriendsScoreByModule(
        rankingDtoRequest: RankingDtoRequest
    ): RankingDtoResponse = client.get("$url/api/score/friends") {
        parameter("moduleType", rankingDtoRequest.moduleType)
    }

}