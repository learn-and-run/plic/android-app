package fr.learnandrun.android.repository.user.dto.request

import kotlinx.serialization.Serializable

@Serializable
data class DeclineInvitationDtoRequest(
    val invitationId: Long
)