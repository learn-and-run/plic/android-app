package fr.learnandrun.android.repository.user.dto.response

import fr.learnandrun.android.service.model.user.UserType
import kotlinx.serialization.Serializable

@Serializable
data class PendingInvitationDto(
    val id: Long,
    val user: UserInfoDto,
    val userType: UserType
)