package fr.learnandrun.android.repository.user.dto

import fr.learnandrun.android.service.model.user.UserType
import kotlinx.serialization.KSerializer
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement
import kotlinx.serialization.json.jsonObject
import kotlinx.serialization.json.jsonPrimitive

object UserDtoSerializer : JsonContentPolymorphicSerializer<UserDto>(UserDto::class) {
    override fun selectDeserializer(element: JsonElement): KSerializer<out UserDto> =
        when (element.jsonObject["userType"]?.jsonPrimitive?.content) {
            UserType.STUDENT.name -> UserDto.Student.serializer()
            UserType.PARENT.name -> UserDto.Parent.serializer()
            UserType.PROFESSOR.name -> UserDto.Professor.serializer()
            else -> throw IllegalStateException("The object must contains userType field with an appropriate value")
        }
}