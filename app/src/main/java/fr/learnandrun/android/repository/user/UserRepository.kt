package fr.learnandrun.android.repository.user

import fr.learnandrun.android.business.KtorClient
import fr.learnandrun.android.repository.Repository
import fr.learnandrun.android.repository.user.dto.request.*
import fr.learnandrun.android.repository.user.dto.response.*
import io.ktor.client.request.*
import io.ktor.client.request.forms.*
import io.ktor.http.*

class UserRepository(
    ktorClient: KtorClient
) : Repository {

    private val client = ktorClient.client
    private val url = "${ktorClient.url}/api"
    private val urlUser = "$url/user"

    suspend fun login(userLoginDtoRequest: UserLoginDtoRequest): Unit =
        client.post("$url/login") {
            body = FormDataContent(Parameters.build {
                parameter("username", userLoginDtoRequest.username)
                parameter("password", userLoginDtoRequest.password)
                parameter("remember-me", userLoginDtoRequest.rememberMe.toString())
            })
        }

    suspend fun logout(): Unit = client.delete("$url/logout")

    suspend fun delete(): Unit = client.delete("$urlUser/delete")

    suspend fun isAuth(): IsAuthDtoResponse = client.get("$urlUser/is_auth")

    suspend fun isFullyAuth(): IsFullyAuthDtoResponse = client.get("$urlUser/is_fully_auth")

    suspend fun getInfo(): UserInfoDtoResponse = client.get("$urlUser/info")

    suspend fun createStudent(
        createStudentDtoRequest: CreateStudentDtoRequest
    ): CreateStudentDtoResponse = client.post("$urlUser/create_student") {
        contentType(ContentType.Application.Json)
        body = createStudentDtoRequest
    }

    suspend fun createParent(
        createParentDtoRequest: CreateParentDtoRequest
    ): CreateParentDtoResponse = client.post("$urlUser/create_parent") {
        contentType(ContentType.Application.Json)
        body = createParentDtoRequest
    }

    suspend fun createProfessor(
        createProfessorDtoRequest: CreateProfessorDtoRequest
    ): CreateProfessorDtoResponse = client.post("$urlUser/create_professor") {
        contentType(ContentType.Application.Json)
        body = createProfessorDtoRequest
    }

    suspend fun updateStudent(
        updateStudentDtoRequest: UpdateStudentDtoRequest
    ): UpdateStudentDtoResponse = client.patch("$urlUser/update/student") {
        contentType(ContentType.Application.Json)
        body = updateStudentDtoRequest
    }

    suspend fun updateParent(
        updateParentDtoRequest: UpdateParentDtoRequest
    ): UpdateParentDtoResponse = client.patch("$urlUser/update/parent") {
        contentType(ContentType.Application.Json)
        body = updateParentDtoRequest
    }

    suspend fun updateProfessor(
        updateProfessorDtoRequest: UpdateProfessorDtoRequest
    ): UpdateProfessorDtoResponse = client.patch("$urlUser/update/professor") {
        contentType(ContentType.Application.Json)
        body = updateProfessorDtoRequest
    }

    suspend fun updateEmail(updateEmailDtoRequest: UpdateEmailDtoRequest): Unit =
        client.patch("$urlUser/update/email") {
            contentType(ContentType.Application.Json)
            body = updateEmailDtoRequest
        }

    suspend fun updatePassword(updatePasswordDtoRequest: UpdatePasswordDtoRequest): Unit =
        client.patch("$urlUser/update/password") {
            contentType(ContentType.Application.Json)
            body = updatePasswordDtoRequest
        }

    suspend fun askResetPassword(askResetPasswordDtoRequest: AskResetPasswordDtoRequest): Unit =
        client.post("$urlUser/reset") {
            contentType(ContentType.Application.Json)
            body = askResetPasswordDtoRequest
        }

    suspend fun getAllRelations(
        allRelationsDtoRequest: AllRelationsDtoRequest
    ): AllRelationsDtoResponse = client.get("$urlUser/relation/all") {
        parameter("userType", allRelationsDtoRequest.userType)
    }

    suspend fun addRelation(
        addRelationDtoRequest: AddRelationDtoRequest
    ): Unit = client.post("$urlUser/relation/add") {
        contentType(ContentType.Application.Json)
        body = addRelationDtoRequest
    }

    suspend fun validateRelation(
        validateRelationRequest: ValidateRelationRequest
    ): Unit = client.post("$urlUser/relation/validate") {
        contentType(ContentType.Application.Json)
        body = validateRelationRequest
    }

    suspend fun getPendingRelations(): PendingInvitationsDtoResponse = client.get("$urlUser/relation/pending")

    suspend fun declineInvitation(
        declineInvitationDtoRequest: DeclineInvitationDtoRequest
    ): Unit = client.delete("$urlUser/relation/decline/${declineInvitationDtoRequest.invitationId}")

    suspend fun deleteRelation(
        deleteRelationDtoRequest: DeleteRelationDtoRequest
    ): Unit = client.delete("$urlUser/relation/delete/${deleteRelationDtoRequest.relationId}")

}