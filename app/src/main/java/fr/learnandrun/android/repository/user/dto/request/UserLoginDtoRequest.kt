package fr.learnandrun.android.repository.user.dto.request

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class UserLoginDtoRequest(
    val username: String,
    val password: String,
    @SerialName("remember-me")
    val rememberMe: Boolean
)