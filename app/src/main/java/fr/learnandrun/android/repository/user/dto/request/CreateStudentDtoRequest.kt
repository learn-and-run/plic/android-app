package fr.learnandrun.android.repository.user.dto.request

import fr.learnandrun.android.service.model.user.LevelType
import kotlinx.serialization.Serializable

@Serializable
data class CreateStudentDtoRequest(
    val pseudo: String,
    val firstname: String,
    val lastname: String,
    val password: String,
    val email: String,
    val levelType: LevelType
)